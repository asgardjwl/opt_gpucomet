import h5py, mdts
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import write_results
from lib.tool import PML
from lib.tool import interp
from lib.tool import gradient_check
from lib.ldos import ldos_slab
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib.solver import Solver
from lib.filter import Filter

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-6)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init', action="store", type=int, default=10)
parser.add_argument('-init_type', action="store", type=str, default='zero')
parser.add_argument('-outputfield', action="store", type=int, default=0)
# filter
parser.add_argument('-bproj', action="store", type=float, default=0.0)
parser.add_argument('-R', action="store", type=int, default=0)
parser.add_argument('-alpha', action="store", type=float, default=1.0)
parser.add_argument('-rep', action="store", type=int, default=1)
# Job
parser.add_argument('-Job', action="store", type=int, default=1)
parser.add_argument('-outputbase', action="store", type=int, default=2)
parser.add_argument('-export_in_object', action="store", type=int, default=1)
parser.add_argument('-maxeval', action="store", type=int, default=5000)
# temporal
parser.add_argument('-freq', action="store", type=float, default=1.0)
parser.add_argument('-Qabs', action="store", type=float, default=200)
# geometry
parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mx', action="store", type=int, default=25)
parser.add_argument('-My', action="store", type=int, default=25)
parser.add_argument('-Mz', action="store", type=int, default=20)
parser.add_argument('-Mzslab', action="store", type=int, default=1)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epssub', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff', action="store", type=float, default=11.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='vac')

# current source
parser.add_argument('-Jamp', action="store", type=float, default=1.0)
parser.add_argument('-Jdir', action="store", type=int, default=2)
parser.add_argument('-cx', action="store", type=int, default=37)
parser.add_argument('-cy', action="store", type=int, default=37)
parser.add_argument('-cz', action="store", type=int, default=35)

r, unknown = parser.parse_known_args(sys.argv[1:])
if comm.rank==0:
    for arg in vars(r):
        print arg," is ",getattr(r,arg)
        
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.outputbase=r.outputbase
verbose.export_in_object=r.export_in_object
verbose.init=r.init
verbose.init_type=r.init_type
verbose.outputfield=r.outputfield
# ----------assembling parameters------------#
hxyz=r.hx*r.hy*r.hz
if r.Qabs>1e10:
    r.Qabs=float('inf')
    
omega = dtype(r.freq*2*pi) - 1j*dtype(r.freq*2*pi/2/r.Qabs)
shape = (r.Nx,r.Ny,r.Nz)
# pml
pmlx=PML(r.Npmlx,omega,r.Nx,r.hx)
pmly=PML(r.Npmly,omega,r.Ny,r.hy)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz)

pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]

#interp matrix
ndof=(r.Mx/r.rep)*(r.My/r.rep)
Mx0= (r.Nx-r.Mx)/2
My0= (r.Ny-r.My)/2
Mz0= (r.Nz-r.Mz)/2
A = interp(r.Mx,r.My,r.Mz,r.Mzslab,r.Nx,r.Ny,r.Nz,Mx0,My0,Mz0,r.rep)

if r.epstype == 'one':
    dof=np.ones([r.Mx/r.rep,r.My/r.rep]).astype(np.float)
elif r.epstype == 'rand':
    dof=np.random.random(r.Mx/r.rep,r.My/r.rep).astype(np.float)
elif r.epstype == 'file':
    f = open(r.epsfile, 'r')
    dof = np.loadtxt(f).reshape(r.Mx/r.rep,r.My/r.rep)
elif r.epstype == 'vac':
    dof=np.zeros([r.Mx/r.rep,r.My/r.rep]).astype(np.float)

if comm.rank == 0:
    print "The size of the problem is",shape
    # current source
    j=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)]);
    j[r.Jdir][r.cx,r.cy,r.cz]=r.Jamp/r.hx/r.hy #electric current

    # epsilon profile
    epsBkg = np.ones(shape)*r.epsbkg
    epsBkg[:,:,:A.Mz0] = r.epssub

    ep = np.copy(epsBkg)
    A.matA(dof,ep,r.epsdiff)

    f=h5py.File('epsF.h5','w')
    f.create_dataset('data',data=ep)
    f.close()

    # initial solution
    x=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    b=[-1j*omega*np.copy(z) for z in j] #b=-i omega j
        
else:
    j=[None] * 3
    x=[None] * 3
    b=[None] * 3
    epsBkg=None
        
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)
# solver
if r.Job is 0:
    solver = Solver(shape, r.tol, r.maxit, pml_p, pml_d, omega)
else:
    solver = Solver(shape, r.tol, r.maxit, pml_d, pml_d, omega)

FT = Filter(r.Mx/r.rep,r.My/r.rep,r.bproj,r.R,r.alpha)
if comm.rank==0:
    b=solver.pre_cond(b)
#-------solve-----------#
grad=np.zeros(ndof)
ldos_old=ldos_slab(shape,j,x,b,epsBkg,r.epsdiff,solver,A,hxyz,omega)
ldos_nlopt = FT.new_object(ldos_old)

grad=np.array([])

def get_reward(struct):
    dof=np.array(struct)
    val=ldos_nlopt(dof,grad)
    return val

atom=[0,1]
myTree=mdts.Tree(no_positions=ndof, atom_types=atom, get_reward=get_reward, positions_order="direct", max_flag=True,expand_children=2, play_out=5, play_out_selection="best", candidate_pool_size=20,ucb="mean", use_combo=True, combo_play_out=20, combo_init_random=5, combo_step=5, combo_lvl=5)

res=myTree.search(display=True,no_candidates=r.maxeval)
print res.optimal_fx
