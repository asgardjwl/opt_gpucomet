import h5py, sys, cmath
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import os, psutil
from gce import verbose
from scipy.sparse import csr_matrix as csr
from scipy import ndimage
from math import exp
from scipy.interpolate import RegularGridInterpolator

def conditioners(pml_p,pml_d, dtype):
    """
    pre- and post- precoonditioner to symmetrize matrix M
    """
    def reshaper(f):
        for k in range(3):
            new_shape = [1, 1, 1]
            new_shape[k] = f[k].size
            f[k] = f[k].reshape(new_shape)
        return f
    # Consts that are used.
    sqrt_sc_pml_0 = reshaper([dtype(np.sqrt(x)**1) for x in pml_p])
    sqrt_sc_pml_1 = reshaper([dtype(np.sqrt(x)**1) for x in pml_d])
    inv_sqrt_sc_pml_0 = reshaper([dtype(np.sqrt(x)**-1) for x in pml_p])
    inv_sqrt_sc_pml_1 = reshaper([dtype(np.sqrt(x)**-1) for x in pml_d])
    # Define the actual functions.
    def apply_cond(x, t0, t1):
        x[0] *= t1[0] * t0[1] * t0[2]
        x[1] *= t0[0] * t1[1] * t0[2]
        x[2] *= t0[0] * t0[1] * t1[2]
        return x

    def pre_step(x):
        return apply_cond(x, sqrt_sc_pml_0, sqrt_sc_pml_1)

    def post_step(x):
        return apply_cond(x, inv_sqrt_sc_pml_0, inv_sqrt_sc_pml_1)

    return pre_step, post_step

def conditioners2(pml_p,pml_d, dtype):
    """
    pre- and post- precoonditioner to symmetrize matrix M
    """
    def pre_step(x):
        return x

    def post_step(x):
        return x

    return pre_step, post_step

    
def write_results(filename,x):
    f=h5py.File(filename,'w')
    for k in range(3):
        data=np.real(x[k]).astype(np.float32)
        d=f.create_dataset('xyz'[k]+'r',data=data)

        data=np.imag(x[k]).astype(np.float32)
        d=f.create_dataset('xyz'[k]+'i',data=data)
    f.close()

def initialize(x, count, init, init_type, shape, dtype):
    if count<init:
        if comm.rank == 0:
            print "   Now at step ", count, " out of ",init," are initialized as ",init_type
        if init_type == 'zero':
            x[:] = list(dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)]))
        elif init_type == 'rand':
            x[:] = list(dtype([np.random.randn(*shape),np.random.randn(*shape),np.random.randn(*shape)]))
            for i in range(3):
                x[i] /= np.linalg.norm(x[i])*1000
        else:
            return 0
    return 0
            
class PML:
    def __init__(self, NPML, omega, N, hx, m=4.0, R=16.0,casimir=False):
        if NPML>0:
            sigma=(m+1)/2/(NPML*hx)*R
            xd=np.linspace(1, N, N)*hx
            xp=xd-0.5*hx

            # prime
            lower=NPML*hx-xp
            upper=xp-(N-NPML)*hx

            l = lower*(lower>0)+upper*(upper>0);
            self.sp=1-1j*(sigma / omega) * (l / NPML/ hx)**m;
        
            # dual
            lower=NPML*hx-xd;
            upper=xd-(N-NPML)*hx;

            l = lower*(lower>0)+upper*(upper>0);
            self.sd=1-1j*(sigma / omega) * (l / NPML/hx)**m;
        else:
            self.sp=np.ones(N)
            self.sd=np.ones(N)

def Forward_Difference(v,shape,hx,hy,hz,ax='x',kx=0.,ky=0.,kz=0.):
    padf = (((0,1),(0,0),(0,0)),  ((0,0),(0,1),(0,0)),  ((0,0),(0,0),(0,1)))
    if ax == 'x':
        u = np.pad(v,padf[0],'wrap')
        u[shape[0],:,:] *= np.exp(-1j*hx*shape[0]*kx)
        return np.diff(u,axis=0)/hx
    elif ax == 'y':
        u = np.pad(v,padf[1],'wrap')
        u[:,shape[1],:] *= np.exp(-1j*hy*shape[1]*ky)
        return np.diff(u,axis=1)/hy
    elif ax == 'z':
        u = np.pad(v,padf[2],'wrap')
        u[:,:,shape[2]] *= np.exp(-1j*hz*shape[2]*kz)
        return np.diff(u,axis=2)/hz        

def E2H(e,omega,shape,hx,hy,hz,kx=0.,ky=0.,kz=0.):
    # Hx = i/omega (dy Ez - dz Ey)
    dyEz = Forward_Difference(e[2],shape,hx,hy,hz,ax='y',kx=kx,ky=ky,kz=kz)
    dzEy = Forward_Difference(e[1],shape,hx,hy,hz,ax='z',kx=kx,ky=ky,kz=kz)
    Hx = 1j/omega*(dyEz-dzEy)

    # Hy = i/omega (dz Ex - dx Ez)
    dzEx = Forward_Difference(e[0],shape,hx,hy,hz,ax='z',kx=kx,ky=ky,kz=kz)
    dxEz = Forward_Difference(e[2],shape,hx,hy,hz,ax='x',kx=kx,ky=ky,kz=kz)
    Hy = 1j/omega*(dzEx-dxEz)
    # Hz = i/omega (dx Ey - dy Ex)    
    dxEy = Forward_Difference(e[1],shape,hx,hy,hz,ax='x',kx=kx,ky=ky,kz=kz)
    dyEx = Forward_Difference(e[0],shape,hx,hy,hz,ax='y',kx=kx,ky=ky,kz=kz)
    Hz = 1j/omega*(dxEy-dyEx)

    return [Hx,Hy,Hz]

def Poynting(e,h):
    # Sx = 0.5 Re (Ey* Hz - Ez* Hy)
    Sx = 0.5*np.real(np.conj(e[1])*h[2]-np.conj(e[2])*h[1])
    # Sy = 0.5 Re (Ez* Hx - Ex* Hz)
    Sy = 0.5*np.real(np.conj(e[2])*h[0]-np.conj(e[0])*h[2])    
    # Sz = 0.5 Re (Ex* Hy - Ey* Hx)
    Sz = 0.5*np.real(np.conj(e[0])*h[1]-np.conj(e[1])*h[0])    

    return [Sx,Sy,Sz]
    
class bloch:
    def __init__(self, shape, hx, hy, hz, kx, ky, kz):
        dtype=np.complex128
        self.back = [np.ones(f,dtype) for f in shape]
        self.forward = [np.ones(f,dtype) for f in shape]

        self.forward[0][-1]=cmath.exp(-1j*hx*shape[0]*kx)
        self.back[0][0]=cmath.exp(1j*hx*shape[0]*kx)
            
        self.forward[1][-1]=cmath.exp(-1j*hy*shape[1]*ky)
        self.back[1][0]=cmath.exp(1j*hy*shape[1]*ky)

        self.forward[2][-1]=cmath.exp(-1j*hz*shape[2]*kz)
        self.back[2][0]=cmath.exp(1j*hz*shape[2]*kz)

class interp:
    def __init__(self,Mx,My,Mz,Mzslab,Nx,Ny,Nz,Mx0,My0,Mz0,rep=1):
        self.Mx=Mx
        self.My=My
        self.Mz=Mz
        self.Mzslab=Mzslab
        self.Mx0=Mx0
        self.My0=My0
        self.Mz0=Mz0
        self.Nx=Nx
        self.Ny=Ny
        self.Nz=Nz
        self.rep = rep
        
    def matA(self,dof,ep,epsdiff):
        if self.Mzslab is 1:
            for i in range(self.Mz0,self.Mz0+self.Mz):
                for kx in range(self.rep):
                    for ky in range(self.rep):
                        ep[kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,i] += dof*epsdiff
        else:
            for kx in range(self.rep):
                for ky in range(self.rep):
                    for kz in range(self.rep):            
                        ep[kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,kz+self.Mz0:self.Mz0+self.Mz:self.rep] += dof*epsdiff
                        
    def matAT(self,dof,e,factor=1.):
        if self.Mzslab is 1:
            for i in range(self.Mz0,self.Mz0+self.Mz):
                for j in range(3):
                    for kx in range(self.rep):
                        for ky in range(self.rep):
                            dof += factor*e[j][kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,i]
        else:
            for j in range(3):
                for kx in range(self.rep):
                    for ky in range(self.rep):
                        for kz in range(self.rep):                
                            dof += factor*e[j][kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,kz+self.Mz0:self.Mz0+self.Mz:self.rep]

def gradient_check(fun, dof, grad, index=0, Ntotal=10, start=0.0, dx=1e-2):
    doftmp=np.copy(dof)
    for tmp in np.linspace(start,start+(Ntotal-1)*dx,Ntotal):
        doftmp[index]=tmp
        val=fun(doftmp,grad)
        if comm.rank==0:
            print "At point",index," with dof = ",doftmp[index], ", val =",val,", gradient =",grad[index]
    
def memory_report(ST=''):
    process = psutil.Process(os.getpid())
    h=process.memory_info().rss/1024/1024.0
    print ST+ 'At core',str(comm.rank),' Used Memory = ' + str(h) +' MB'

def generateE(shape):
    a=np.random.randn(*shape)
    a[0,:,:]=a[-1,:,:]
    a[:,0,:]=a[:,-1,:]
    a[:,:,0]=a[:,:,-1]
    # n=5
    # a[0:n,:,:]=0
    # a[-n:-1,:,:]=0
    # a[:,0:n,:]=0
    # a[:,-n:-1,:]=0
    # a[:,:,0:n]=0
    # a[:,:,-n:-1]=0    

    return a

class Multigrid:
    def __init__(self,shape,gridx,gridy,gridz):
        self.shape = shape
        self.grid = gridx
        self.gridx = gridx
        self.gridy = gridy
        self.gridz = gridz
        self.shape_c = (shape[0]/gridx,shape[1]/gridy,shape[2]/gridz)
        
    # def Restriction(self,vec):
    #     vec_c = [np.complex128(np.zeros(self.shape_c)) for i in range(3)]
        
    #     grid = self.grid
    #     N1 = self.shape_c[0]*grid
    #     N2 = self.shape_c[1]*grid
    #     N3 = self.shape_c[2]*grid
        
    #     for p in range(3):
    #         for i in range(grid):
    #             for j in range(grid):
    #                 for k in range(grid):
    #                     vec_c[p] += vec[p][i:N1:grid,j:N2:grid,k:N3:grid]
                        
    #         vec_c[p] /= grid**3
            
    #     return vec_c

    # def Prolongation(self,vec_c):
    #     vec = [np.complex128(np.zeros(self.shape)) for i in range(3)]
        
    #     grid = self.grid
    #     N1 = self.shape_c[0]*grid
    #     N2 = self.shape_c[1]*grid
    #     N3 = self.shape_c[2]*grid

    #     for p in range(3):
    #         for i in range(grid):
    #             for j in range(grid):
    #                 for k in range(grid):
    #                     vec[p][i:N1:grid,j:N2:grid,k:N3:grid] = vec_c[p]
    #     return vec

    def Restriction_linear(self,vec):
        vec_c = []
        
        gridx = self.gridx
        gridy = self.gridy
        gridz = self.gridz
        N1 = (self.shape_c[0]-1)*gridx
        N2 = (self.shape_c[1]-1)*gridy
        N3 = (self.shape_c[2]-1)*gridz       

        xc = np.linspace((gridx-1)/2.0,N1+(gridx-1)/2.0,self.shape_c[0])
        yc = np.linspace((gridy-1)/2.0,N2+(gridy-1)/2.0,self.shape_c[1])
        zc = np.linspace((gridz-1)/2.0,N3+(gridz-1)/2.0,self.shape_c[2])

        x = np.linspace(0,self.shape[0]-1,self.shape[0])
        y = np.linspace(0,self.shape[1]-1,self.shape[1])
        z = np.linspace(0,self.shape[2]-1,self.shape[2])
        # xc = np.linspace(0,1,self.shape_c[0])
        # yc = np.linspace(0,1,self.shape_c[1])
        # zc = np.linspace(0,1,self.shape_c[2])

        # x = np.linspace(0,1,self.shape[0])
        # y = np.linspace(0,1,self.shape[1])
        # z = np.linspace(0,1,self.shape[2])
        xp,yp,zp = np.meshgrid(xc, yc, zc, indexing='ij',sparse='True')

        for p in range(3):
            interp = RegularGridInterpolator((x, y, z), vec[p],method='linear',bounds_error=0,fill_value=0)
            vec_c.append(interp((xp,yp,zp)))
            
        return vec_c
        
    def Prolongation_linear(self,vec_c):
        vec = []
        
        gridx = self.gridx
        gridy = self.gridy
        gridz = self.gridz
        N1 = (self.shape_c[0]-1)*gridx
        N2 = (self.shape_c[1]-1)*gridy
        N3 = (self.shape_c[2]-1)*gridz       

        xc = np.linspace((gridx-1)/2.0,N1+(gridx-1)/2.0,self.shape_c[0])
        yc = np.linspace((gridy-1)/2.0,N2+(gridy-1)/2.0,self.shape_c[1])
        zc = np.linspace((gridz-1)/2.0,N3+(gridz-1)/2.0,self.shape_c[2])

        x = np.linspace(0,self.shape[0]-1,self.shape[0])
        y = np.linspace(0,self.shape[1]-1,self.shape[1])
        z = np.linspace(0,self.shape[2]-1,self.shape[2])
        # xc = np.linspace(0,1,self.shape_c[0])
        # yc = np.linspace(0,1,self.shape_c[1])
        # zc = np.linspace(0,1,self.shape_c[2])

        # x = np.linspace(0,1,self.shape[0])
        # y = np.linspace(0,1,self.shape[1])
        # z = np.linspace(0,1,self.shape[2])
        xp,yp,zp = np.meshgrid(x, y, z, indexing='ij',sparse='True')

        for p in range(3):
            interp = RegularGridInterpolator((xc, yc, zc), vec_c[p],method='linear',bounds_error=0,fill_value=0)
            vec.append(interp((xp,yp,zp)))
        return vec
        

def set_lap(field,shape,p2,val0):
    lap = [None]*3
    R0=5
    dtype=np.complex128
    for k in range(verbose.lap_y0-R0,verbose.lap_y0+R0):
        lap = dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
        lap[p2][verbose.lap_x0:verbose.lap_x0+verbose.lap_tx,k:k+verbose.lap_ty,verbose.lap_z0:verbose.lap_z0+verbose.lap_tz]=1

        val=0            
        for i in range(3):
            val += np.vdot(lap[i],field[i])
        phase = -cmath.phase(val)
        val = np.real(val*cmath.exp(phase*1j))
        print "++++  at  dy = ",k-verbose.lap_y0,", val = ",val

        if val>val0:
            print "        updated  to y = ",k
            verbose.lap=lap
            val0=val
    return 1
