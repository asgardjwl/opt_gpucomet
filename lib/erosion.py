import numpy as np
from scipy.sparse import csr_matrix as csr
from math import exp
dtype=np.complex128

class interp_erosion:
    def __init__(self,Mx,My,Mz,Nx,Ny,Nz,Mx0,My0,Mz0,angle=0.,scale=100.,bproj=0.,Yee=True,mode='dilation',fromlist=False):
        '''
        angle = 0: verticle
        '''
        self.Mx=Mx
        self.My=My
        self.Mz=Mz
        self.Mx0=Mx0
        self.My0=My0
        self.Mz0=Mz0
        self.Nx=Nx
        self.Ny=Ny
        self.Nz=Nz
        self.slope = np.tan(angle*np.pi/180.)
        self.Ero = Erosion(Mx=Mx,My=My,scale=scale)
        self.bproj = bproj
        self.Yee = Yee
        self.mode = mode
        self.scale = scale
        self.fromlist = fromlist

    def matAfromlist(self,dof,ep,epsdiff):
        Ero = Erosion(Mx=self.Mx,My=self.My,scale=np.abs(self.scale))
        dilate = Erosion(Mx=self.Mx,My=self.My,scale=-np.abs(self.scale))
        if type(epsdiff) != list:
            epsdiff = [epsdiff for i in range(3)]
        dof3d = np.zeros((self.Mx,self.My,self.Mz),dtype=float)
        
        dofshape = np.copy(dof).reshape((self.Mx,self.My))
        for i in range(self.Mz):
            if self.fromlist[1]>0:
                if i<self.fromlist[0]:
                    dof3d[:,:,i] = dilate.f_erosion(dofshape)
                elif i<self.fromlist[1] and i>=self.fromlist[0]:
                    dof3d[:,:,i] = dofshape
                else:
                    dof3d[:,:,i] = Ero.f_erosion(dofshape)
            else:
                dof3d[:,:,i] = dofshape
        
        for i in range(3):
            ep[i][self.Mx0:self.Mx0+self.Mx,self.My0:self.My0+self.My,self.Mz0:self.Mz0+self.Mz] += dof3d*epsdiff[i]

        if self.Yee == True:
            ep[0][0:-1,:,:]=0.5*(ep[0][0:-1,:,:]+ep[0][1:,:,:])
            ep[1][:,0:-1,:]=0.5*(ep[1][:,0:-1,:]+ep[1][:,1:,:])
            ep[2][:,:,0:-1]=0.5*(ep[2][:,:,0:-1]+ep[2][:,:,1:])

            
    def matA(self,dof,ep,epsdiff):
        if type(epsdiff) != list:
            epsdiff = [epsdiff for i in range(3)]
        dof3d = np.zeros((self.Mx,self.My,self.Mz),dtype=float)

        if self.mode == 'erosion':
            dof3d[:,:,0] = np.copy(dof).reshape((self.Mx,self.My))
        elif self.mode == 'dilation':
            dof3d[:,:,-1] = np.copy(dof).reshape((self.Mx,self.My))
            
        for i in range(1,self.Mz):
            if self.mode == 'erosion':
                il = i
                di = 1
            elif self.mode == 'dilation':
                il = self.Mz-1-i
                di = -1
            narrow = int(round(self.slope*il))
            narrowp = int(round(self.slope*(il-di)))            
            if narrow != narrowp:
                dof3d[:,:,il] = self.Ero.f_erosion(dof3d[:,:,il-di])
            else:
                dof3d[:,:,il] = np.copy(dof3d[:,:,il-di])
        dof3d,projgrad = self.Ero.f_proj(self.bproj,dof3d)
        
        for i in range(3):
            ep[i][self.Mx0:self.Mx0+self.Mx,self.My0:self.My0+self.My,self.Mz0:self.Mz0+self.Mz] += dof3d*epsdiff[i]

        if self.Yee == True:
            ep[0][0:-1,:,:]=0.5*(ep[0][0:-1,:,:]+ep[0][1:,:,:])
            ep[1][:,0:-1,:]=0.5*(ep[1][:,0:-1,:]+ep[1][:,1:,:])
            ep[2][:,:,0:-1]=0.5*(ep[2][:,:,0:-1]+ep[2][:,:,1:])
    
        return projgrad
    
    def matAT(self,g,dof,epsdiff,projgrad):
        if type(epsdiff) != list:
            epsdiff = [epsdiff for i in range(3)]        
        gtmp = [np.copy(f) for f in g]
        if self.Yee == True:
            gtmp[0][1:,:,:] = 0.5*(g[0][0:-1,:,:]+g[0][1:,:,:])
            gtmp[1][:,1:,:] = 0.5*(g[1][:,0:-1,:]+g[1][:,1:,:])
            gtmp[2][:,:,1:] = 0.5*(g[2][:,:,0:-1]+g[2][:,:,1:])

        gdoftmp = np.zeros((self.Mx,self.My,self.Mz),dtype=dtype)
        for i in range(3):
            gdoftmp += epsdiff[i]*gtmp[i][self.Mx0:self.Mx0+self.Mx,self.My0:self.My0+self.My,self.Mz0:self.Mz0+self.Mz]
        gdoftmp *=projgrad
        
        dof3d = np.zeros((self.Mx,self.My,self.Mz),dtype=dtype)
        gdof3d = np.zeros((self.Mx*self.My,self.Mz),dtype=dtype)

        if self.mode == 'erosion':
            dof3d[:,:,0] = np.copy(dof).reshape((self.Mx,self.My))
            gdof3d[:,0] = np.copy(gdoftmp[:,:,0].flatten())
        elif self.mode == 'dilation':
            dof3d[:,:,-1] = np.copy(dof).reshape((self.Mx,self.My))
            gdof3d[:,-1] = np.copy(gdoftmp[:,:,-1].flatten())

        for i in range(1,self.Mz):
            if self.mode == 'erosion':
                il = i
                di = 1
            elif self.mode == 'dilation':
                il = self.Mz-1-i
                di = -1            
            narrowpi = int(round(self.slope*(il-di)))
            narrowi = int(round(self.slope*il))
            if narrowi != narrowpi:
                dof3d[:,:,il],grad = self.Ero.f_erosion(dof3d[:,:,il-di],gdoftmp[:,:,il].flatten())
                gdof3d[:,il]=np.copy(grad)
            else:
                dof3d[:,:,il] = np.copy(dof3d[:,:,il-di])
                gdof3d[:,il]=np.copy(gdoftmp[:,:,il].flatten())
                
            for j in range(i-1,0,-1):
                if self.mode == 'erosion':
                    ij = j
                elif self.mode == 'dilation':
                    ij = self.Mz-1-j
                narrowpj = int(round(self.slope*ij))
                narrowj = int(round(self.slope*(ij-di)))
                if narrowj != narrowpj:
                    _,grad = self.Ero.f_erosion(dof3d[:,:,ij-di],gdof3d[:,il])
                    gdof3d[:,il] = np.copy(grad)
        return np.sum(gdof3d,axis=1)
        
class Erosion:
    def __init__(self,Mx=10,My=10,scale=100.):
        self.Mx = Mx
        self.My = My
        self.scale = scale

        self.R = 1
        self.n0 = 2*self.R+1
        self.ngrid = np.zeros(Mx*My,dtype=int)
        self.Dsum=self.f_sum()        

    def f_erosion(self,x,grad=None):
        '''
        -1/scale log sum exp(-scale*x) + offset, offset so that it's [0,1]
        offset = log n0^2/scale

        if grad is not None, it should be a numpy vector
        '''
        yexp = np.exp(-self.scale*x.flatten())
        ysum = self.Dsum.dot(yexp)
        yout = -np.log(ysum)/self.scale+np.log(self.ngrid)/self.scale
        y = yout.reshape([self.Mx,self.My])

        if grad is None:
            return y
        else:
            g1 = -grad/ysum/self.scale
            g2 = self.Dsum.transpose().dot(g1)
            gy = g2*-self.scale*yexp
            return y,gy
        
    # sum function over neighbors
    def f_sum(self):
        dim = self.Mx*self.My
        R = self.R
        n0 = self.n0
        indptr = [0]
        indices = []
        data = []

        ctrlt = 0
        for i in range(dim):
            xi = int(i/self.My)
            yi = i%self.My
            norm = 0.0
            ctrl = 0
            dtmp = []
            
            for j in range(n0**2):
                xj = int(j/n0)+xi-R
                yj = j%n0+yi-R

                if xj<0 or xj>=self.Mx or yj<0 or yj>=self.My:
                    continue

                ctrl += 1
                indices.append(xj*self.My+yj)
                weight = 1.
                dtmp.append(weight)
            data += dtmp
            ctrlt += ctrl
            self.ngrid[i] = ctrl
            indptr.append(ctrlt)

        D = csr((data, indices, indptr), shape=(dim, dim))
        return D

    # projection filter for binarization
    def f_proj(self,bproj,dof):
        eta = 0.5

        dofnew = np.where(dof<=eta,eta*(np.exp(-bproj*(1-dof/eta))-(1-dof/eta)*np.exp(-bproj)),(1-eta)*(1-np.exp(-bproj*(dof-eta)/(1-eta)) + (dof - eta)/(1-eta) * np.exp(-bproj)) + eta)
        dofgrad = np.where(dof<=eta,eta*((bproj/eta)*np.exp(-bproj*(1-dof/eta))+np.exp(-bproj)/eta),(1-eta)*(bproj/(1-eta)*np.exp(-bproj*(dof-eta)/(1-eta))+np.exp(-bproj)/(1-eta)))
        return dofnew, dofgrad
