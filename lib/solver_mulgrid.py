import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
from gce_mul import verbose
from gce_mul.grid import Grid
from tool import initialize, memory_report
import time
dtype=np.complex128

class Solver_mul:
    def __init__(self, solverf,solverc, m_grid,tol_init=0.1,maxit_add=20000,maxmul=10):
        self.solverf = solverf
        self.solverc = solverc
        self.m_grid = m_grid
        self.tol_init = tol_init
        self.maxit_add = maxit_add
        self.maxmul = maxmul
        self.max_iters = self.solverf.max_iters        

    def coarse(self,b):
        b2 = [None]*3
        x2 = [None]*3
        if comm.rank == 0:
            b2=self.m_grid.Restriction_linear(b)
            b2=self.solverc.pre_cond(b2)
            x2=[dtype(np.zeros(self.solverc.shape)) for i in range(3)]
                     
        initialize(x2, 0, verbose.init, verbose.init_type, self.solverc.shape, dtype)
        bg2 = [Grid(dtype(np.copy(f)), self.solverc.shape_obj,x_overlap=1) for f in b2]
        xg2 = [Grid(dtype(np.copy(f)), self.solverc.shape_obj,x_overlap=1) for f in x2]

        xg2, err, success = self.solverc.bicg(bg2,xg2)

        e2=[E.get() for E in xg2]
        u = [None]*3
        if comm.rank == 0:
            e2=self.solverc.post_cond(e2)
            u=self.m_grid.Prolongation_linear(e2)
            
        return u

    def finer(self,b,xg):
        self.solverf.max_iters = self.max_iters

        bg = [Grid(dtype(np.copy(f)), self.solverf.shape_obj,x_overlap=1) for f in b]
        xg, err, success = self.solverf.bicg(bg,xg)

        if success or err[-1] > self.tol_init:
            return xg, err, success, bg                   

        if comm.rank == 0:
            print "    * refine, step = ", 0
        bg = [Grid(dtype(np.copy(f)), self.solverf.shape_obj,x_overlap=1) for f in b]
        self.solverf.max_iters = self.maxit_add        
        xg, err, success = self.solverf.bicg(bg,xg)

        if success or err[-1] > self.tol_init/10:
            return xg, err, success, bg

        if comm.rank == 0:
            print "    * refine, step = ", 1
        bg = [Grid(dtype(np.copy(f)), self.solverf.shape_obj,x_overlap=1) for f in b]
        self.solverf.max_iters = 2*self.maxit_add        
        xg, err, success = self.solverf.bicg(bg,xg)

        return xg, err, success, bg
                     
    def Multisolve(self, b, xg=None):
        # start from finer
        xg, err, success, bg = self.finer(b,xg)
        if success:
            return xg, err, success
            
        ctrl = 0
        btmp = [None]*3
        while ctrl<self.maxmul:
            btmp=[E.get() for E in bg]
            e=[E.get() for E in xg]
            if comm.rank == 0:
                print "*** Multigrid, iter = ", ctrl
                print "  * coarse solver, size = ", self.solverc.shape
                btmp=self.solverf.post_cond(btmp)
            u = self.coarse(btmp)
            
            if comm.rank == 0:
                u=self.solverf.pre_cond(u)
                for i in range(3):
                    u[i] += e[i]
                print "  * fine solver, size = ", self.solverf.shape
                
            xg = [Grid(dtype(np.copy(f)), self.solverf.shape_obj,x_overlap=1) for f in u]
            xg, err, success,bg = self.finer(b,xg)

            memory_report(str(ctrl)+'-')
            ctrl += 1
            if success:
                return xg, err, success
                
        return xg, err, success            
