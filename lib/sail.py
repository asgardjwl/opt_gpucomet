import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time
from math import pi
import cmath

from tool import memory_report, write_results, initialize, set_lap
from gce.grid import Grid
from gce import verbose
dtype=np.complex128

def gradient_E(field,polarization,shape,hx,hy,hz,kx,ky,kz,g_direction,z_plane):
    field_G=np.zeros(shape)
    z_cut=np.zeros(shape)
    z_cut[:,:,z_plane]=1.0

    x=field[polarization]
    x*= z_cut
    
    if g_direction == 'x':
        field_G[1:-1,:,:]=(x[2:,:,:]-x[:-2,:,:])/hx
        field_G[-1,:,:]=cmath.exp(-1j*hx*shape[0]*kx)*x[0,:,:]-x[
        

class bloch:
    def __init__(self, shape, hx, hy, hz, kx, ky, kz):
        dtype=np.complex128
        self.back = [np.ones(f,dtype) for f in shape]
        self.forward = [np.ones(f,dtype) for f in shape]

        self.forward[0][-1]=cmath.exp(-1j*hx*shape[0]*kx)
        self.back[0][0]=cmath.exp(1j*hx*shape[0]*kx)
            
        self.forward[1][-1]=cmath.exp(-1j*hy*shape[1]*ky)
        self.back[1][0]=cmath.exp(1j*hy*shape[1]*ky)

        self.forward[2][-1]=cmath.exp(-1j*hz*shape[2]*kz)
        self.back[2][0]=cmath.exp(1j*hz*shape[2]*kz)
        
def flux_slab(shape,j,x1,b,epsBkg,epsdiff,epsimag,solver,A,hxyz):
    def flux_nlopt(dof,grad):
