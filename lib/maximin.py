import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
from gce import verbose

def obj_fun(dofall,grad):
    '''
    f([x,t]) = t
    '''
    dofall = comm.bcast(dofall)
    ndof = len(dofall)-1
    t = dofall[ndof]

    verbose.count +=1
    if  verbose.count%verbose.outputbase is 1 and comm.rank == 0:
        dof = dofall[:ndof]
        np.savetxt(verbose.filename+'dofinit'+str(verbose.count)+'.txt', dof)

        if verbose.FT != 0:
            if verbose.FT.bproj>0. or verbose.FT.R>0:
                dofnew = verbose.FT.new_dof(dof)
                np.savetxt(verbose.filename+'dof'+str(verbose.count)+'.txt', dofnew)
                
    if grad.size>0:
        if comm.rank == 0:
            grad[:] = np.append(np.zeros(ndof,dtype=float),1)
        grad = comm.bcast(grad)

    if comm.rank==0:
        print "At count ",verbose.count,", dummyt = ",t
    return t

def funmaximin(fun):
    '''
    f([x,t]) = t - fun(x)
    '''
    def fun_nlopt(dofall,grad):
        ndof = len(dofall)-1
        dof = dofall[:ndof]
        t = dofall[ndof]
           
        if grad.size>0:
            g = np.zeros(ndof,dtype=float)
            val = fun(dof,g)
            grad[:] = np.append(-g,1)
        else:
            g = np.array([])
            val = fun(dof,g)
        return t-val
    return fun_nlopt
            
