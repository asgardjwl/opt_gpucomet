import numpy as npa
import autograd.numpy as np
from autograd import make_vjp
from scipy.sparse import csr_matrix as csr
from functools import partial
from autograd.extend import defvjp,primitive

@primitive
def f_mul(x,D):
    return D.dot(x)

def grad_fmul(argnum,ans,x,D):
    if argnum == 0:
        return lambda g: D.transpose().dot(g)
defvjp(f_mul, partial(grad_fmul,0))

@primitive
def f_mat(dof,ep,Mx,My,Mz,Mx0,My0,Mz0):
    epnew = np.copy(ep)
    epnew[Mx0:Mx0+Mx,My0:My0+My,Mz0:Mz0+Mz] += dof
    return epnew

def grad_fmat(argnum,ans,dof,ep,Mx,My,Mz,Mx0,My0,Mz0):
    if argnum == 0:
        return lambda g: g[Mx0:Mx0+Mx,My0:My0+My,Mz0:Mz0+Mz]
defvjp(f_mat, partial(grad_fmat,0))

class interp_erosion:
    def __init__(self,Mx,My,Mz,Nx,Ny,Nz,Mx0,My0,Mz0,angle=0.,scale=100.,bproj=0.,Yee=True,topbottom='top',pre_erosion=0):
        '''
        angle = 0: verticle
        '''
        self.Mx=Mx
        self.My=My
        self.Mz=Mz
        self.Mx0=Mx0
        self.My0=My0
        self.Mz0=Mz0
        self.Nx=Nx
        self.Ny=Ny
        self.Nz=Nz
        self.slope = np.tan(angle*np.pi/180.)
        self.Ero_erosion = Erosion(Mx=Mx,My=My,scale=scale)
        self.Ero_dilation = Erosion(Mx=Mx,My=My,scale=-scale)
        self.bproj = bproj
        self.Yee = Yee
        self.topbottom = topbottom
        self.pre_erosion = pre_erosion
        
    def matA(self,dof,ep,epsdiff):
        if type(epsdiff) != list:
            epsdiff = [epsdiff for i in range(3)]

        step = int(round(self.slope*(self.Mz-1)))+1
        dofstep1 = []
        dofstep2 = []
        dof3d = []

        if self.pre_erosion == 1:
            dofnew = self.Ero_erosion.f_erosion(dof)
        elif self.pre_erosion == -1:
            dofnew = self.Ero_dilation.f_erosion(dof)
        else:
            dofnew = dof
        
        if self.topbottom == 'top':
            for i in range(step):
                if i == 0:
                    dofstep1.append(dofnew.reshape((self.Mx,self.My)))
                else:
                    dofstep1.append(self.Ero_dilation.f_erosion(dofstep1[i-1]))
                    
            for i in range(step):
                if i == 0:
                    dofstep2.append(dofstep1[step-1])
                else:
                    dofstep2.append(self.Ero_erosion.f_erosion(dofstep2[i-1]))
                    
            ctrl = 0
            for i in range(self.Mz):
                narrow = int(round(self.slope*i))
                narrowp = int(round(self.slope*(i-1)))            
                if i == 0:
                    narrowp = 0
                if narrow != narrowp:
                    ctrl += 1
                dof3d.append(dofstep2[ctrl])
                
        if self.topbottom == 'bottom':
            for i in range(step):
                if i == 0:
                    dofstep1.append(dofnew.reshape((self.Mx,self.My)))
                else:
                    dofstep1.append(self.Ero_erosion.f_erosion(dofstep1[i-1]))
                
            for i in range(step):
                if i == 0:
                    dofstep2.append(dofstep1[step-1])
                else:
                    dofstep2.append(self.Ero_dilation.f_erosion(dofstep2[i-1]))
            ctrl = step-1
            for i in range(self.Mz):
                narrow = int(round(self.slope*i))
                narrowp = int(round(self.slope*(i-1)))            
                if i == 0:
                    narrowp = 0
                if narrow != narrowp:
                    ctrl -= 1
                dof3d.append(dofstep2[ctrl])                

        dof3d = np.array(dof3d)
        dof3d = np.rollaxis(dof3d,0,3)
        dof3d = self.Ero_erosion.f_proj(self.bproj,dof3d)

        epnew = []
        for i in range(3):
            epnew.append(f_mat(dof3d*epsdiff[i],ep[i],self.Mx,self.My,self.Mz,self.Mx0,self.My0,self.Mz0))
            
        if self.Yee == True:
            epnew[0] = 0.5 * (epnew[0] + np.roll(epnew[0],-1,axis=0))
            epnew[1] = 0.5 * (epnew[1] + np.roll(epnew[1],-1,axis=1))
            epnew[2] = 0.5 * (epnew[2] + np.roll(epnew[2],-1,axis=2))
        return np.array(epnew)
    
    def matAT(self,grad,dof,ep,epsdiff):
        fun = lambda x: self.matA(x,ep,epsdiff)
        vjp,val = make_vjp(fun)(dof)
        return vjp(grad)
    
class Erosion:
    def __init__(self,Mx=10,My=10,scale=100.):
        self.Mx = Mx
        self.My = My
        self.scale = scale

        self.R = 1
        self.n0 = 2*self.R+1
        self.ngrid = np.zeros(Mx*My,dtype=int)
        self.Dsum=self.f_sum()        

    def f_erosion(self,x,boundary=0):
        '''
        -1/scale log sum exp(-scale*x) + offset, offset so that it's [0,1]
        offset = log n0^2/scale

        if grad is not None, it should be a numpy vector
        '''
        yexp = np.exp(-self.scale*x.flatten())
        ysum = f_mul(yexp,self.Dsum) + (self.n0**2-self.ngrid)*np.exp(-self.scale*boundary)
        yout = -np.log(ysum)/self.scale+np.log(self.n0**2)/self.scale
        y = np.reshape(yout,[self.Mx,self.My])

        return y
        
    # sum function over neighbors
    def f_sum(self):
        dim = self.Mx*self.My
        R = self.R
        n0 = self.n0
        indptr = [0]
        indices = []
        data = []

        ctrlt = 0
        for i in range(dim):
            xi = int(i/self.My)
            yi = i%self.My
            norm = 0.0
            ctrl = 0
            dtmp = []
            
            for j in range(n0**2):
                xj = int(j/n0)+xi-R
                yj = j%n0+yi-R

                if xj<0 or xj>=self.Mx or yj<0 or yj>=self.My:
                    continue

                ctrl += 1
                indices.append(xj*self.My+yj)
                weight = 1.
                dtmp.append(weight)
            data += dtmp
            ctrlt += ctrl
            self.ngrid[i] = ctrl
            indptr.append(ctrlt)

        D = csr((data, indices, indptr), shape=(dim, dim))
        return D

    # projection filter for binarization
    def f_proj(self,bproj,dof):
        eta = 0.5

        dofnew = np.where(dof<=eta,eta*(np.exp(-bproj*(1-dof/eta))-(1-dof/eta)*np.exp(-bproj)),(1-eta)*(1-np.exp(-bproj*(dof-eta)/(1-eta)) + (dof - eta)/(1-eta) * np.exp(-bproj)) + eta)
        return dofnew
