import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time
from math import pi
import cmath

from tool import memory_report, write_results, initialize
from rsvd import rsvd_singular, rsvd_Vonly
from gce.grid import Grid
from gce import verbose
dtype=np.complex128

def fun_Z(solver,A, B, init_type):
    #Z = B M^-1 A x
    def fun(x):
        y=[None]*3
        xinit=[None]*3
        if comm.rank == 0:
            y=[dtype(np.copy(x[i])*A) for i in range(3)]
            y=solver.pre_cond(y)
            initialize(xinit, 0, 2, init_type, solver.shape, dtype)
        
        xg = [Grid(dtype(f), x_overlap=1) for f in xinit]
        xg, err, success = solver.Multisolve(y, xg)

        field=[E.get() for E in xg]
        if comm.rank==0:
            field=solver.post_cond(field)
            y=[field[i]*B for i in range(3)]
        
        return y
    return fun

def fun_Maxwell(solver, init_type):
    def fun(x):
        xinit=[None]*3
        y=[None]*3
        if comm.rank == 0:
            y=[dtype(np.copy(f)) for f in x]
            y=solver.pre_cond(y)
            initialize(xinit, 0, 2, init_type, solver.shape, dtype)
        
        xg = [Grid(dtype(f), x_overlap=1) for f in xinit]
        xg, err, success = solver.Multisolve(y, xg)

        y=[E.get() for E in xg]
        if comm.rank==0:
            y=solver.post_cond(y)
        
        return y
    return fun    

def fun_Zdagger(solver,A, B, init_type):
    #Z^\dagger = A M^-1^* B x = (A M^-1 B x*)*
    def fun(x):
        y=[None]*3
        xinit=[None]*3
        if comm.rank == 0:
            y=[dtype(np.conj(x[i])*B) for i in range(3)]
            y=solver.pre_cond(y)
            initialize(xinit, 0, 2, init_type, solver.shape, dtype)
        
        xg = [Grid(dtype(f), x_overlap=1) for f in xinit]
        xg, err, success = solver.Multisolve(y, xg)

        field=[E.get() for E in xg]
        if comm.rank==0:
            field=solver.post_cond(field)
            y=[np.conj(field[i])*A for i in range(3)]
        
        return y
    return fun    
    
def RHT_box(shape,epsBkg,epsdiff,epsimag,solver,Aup, Adown, tol, Nmax):
    def RHT_nlopt(dof,grad):
        e=[None]*3
        A=[]
        B=[]
        if comm.rank==0:
            ndof1 = Aup.Mx/Aup.rep*Aup.My/Aup.rep*Aup.Mz/Aup.rep
            doftmp1 = dof[0:ndof1].reshape(Aup.Mx/Aup.rep,Aup.My/Aup.rep,Aup.Mz/Aup.rep)
            doftmp2 = dof[ndof1:].reshape(Adown.Mx/Adown.rep,Adown.My/Adown.rep,Adown.Mz/Adown.rep)
            ep = dtype(np.copy(epsBkg))
            Aup.matA(doftmp1,ep,epsdiff-1j*epsimag)
            Adown.matA(doftmp2,ep,epsdiff-1j*epsimag)            
            e=[ep, ep, ep]
            
            A = dtype(np.zeros_like(epsBkg))
            B = dtype(np.zeros_like(epsBkg))
            Aup.matA(doftmp1,A,epsimag)
            Adown.matA(doftmp2,B,epsimag)
            A=np.sqrt(A)
            B=np.sqrt(B)
            
        # update solvers
        solver.update_ep(e)
        fun = fun_Z(solver,A, B, verbose.init_type)
        fun_dagger = fun_Zdagger(solver,A, B, verbose.init_type)
        s = rsvd_singular(fun, fun_dagger, tol, Nmax, shape)

        val = np.sum(np.abs(s)**2)
        val *= 2*np.real(solver.omega)**4/pi
        return val
    return RHT_nlopt

def RHT_box_gradient(shape,epsBkg,epsdiff,epsimag,solver,Aup, Adown, tol, Nmax):
    def RHT_nlopt(dof,grad):
        e=[None]*3
        A=[]
        B=[]
        if comm.rank==0:
            ndof1 = Aup.Mx/Aup.rep*Aup.My/Aup.rep*Aup.Mz/Aup.rep
            doftmp1 = dof[0:ndof1].reshape(Aup.Mx/Aup.rep,Aup.My/Aup.rep,Aup.Mz/Aup.rep)
            doftmp2 = dof[ndof1:].reshape(Adown.Mx/Adown.rep,Adown.My/Adown.rep,Adown.Mz/Adown.rep)
            ep = dtype(np.copy(epsBkg))
            Aup.matA(doftmp1,ep,epsdiff-1j*epsimag)
            Adown.matA(doftmp2,ep,epsdiff-1j*epsimag)            
            e=[ep, ep, ep]
            
            A = dtype(np.zeros_like(epsBkg))
            B = dtype(np.zeros_like(epsBkg))
            Aup.matA(doftmp1,A,epsimag)
            Adown.matA(doftmp2,B,epsimag)
            A=np.sqrt(A)
            B=np.sqrt(B)
            
        # update solvers
        solver.update_ep(e)
        fun = fun_Z(solver,A, B, verbose.init_type)
        fun_dagger = fun_Zdagger(solver,A, B, verbose.init_type)
        fun_M = fun_Maxwell(solver, verbose.init_type)

        s,V = rsvd_Vonly(fun, fun_dagger, tol, Nmax, shape)
        val = np.sum(np.abs(s)**2)
        val *= 2*np.real(solver.omega)**4/pi

        if comm.rank==0:
            print
            print "+++  At freq = ",np.real(solver.omega)/2/pi," and count ",verbose.count,", RHT = ",val
            print
            if verbose.export_in_object is 1 and verbose.count%verbose.outputbase is 0:
                np.savetxt(verbose.filename+'dof'+str(verbose.count)+'.txt', dof)
        if verbose.export_in_object is 1:
            verbose.count +=1
            
        if grad.size>0:
            if comm.rank == 0:
                grad1=dtype(np.zeros((Aup.Mx/Aup.rep,Aup.My/Aup.rep,Aup.Mz/Aup.rep)))
                grad2=dtype(np.zeros((Adown.Mx/Adown.rep,Adown.My/Adown.rep,Adown.Mz/Adown.rep)))
            for i in range(len(V)):
                # M^-1 A V
                x=[]
                if comm.rank == 0:
                    x = [f*A for f in V[i]]
                MAV = fun_M(x)
                # M^-1 B (ZV)*
                MBZV = fun(V[i])
                if comm.rank == 0:
                    x = [B*np.conj(f) for f in MBZV]
                MBZV = fun_M(x)
                if comm.rank == 0:
                    # g0=2Re[omega^2*epsdiff*MBZV*MAV]
                    g = [2*np.real(solver.omega**2*(epsdiff-1j*epsimag)*MBZV[k]*MAV[k]) for k in range(3)]
                    Aup.matAT(grad1,g)
                    Adown.matAT(grad2,g)

                    # g1=epsimag/s^2*|MBZV|^2
                    if s[i]>1e-90:
                        g = [epsimag/np.abs(s[i])**2*np.abs(f)**2 for f in MBZV]
                        Aup.matAT(grad1,g)

                    # g2=epsimag*|MAV|^2
                    g = [epsimag*np.abs(f)**2 for f in MAV]
                    Adown.matAT(grad2,g)
                    
            if comm.rank == 0:
                grad[:] = np.real(np.concatenate((grad1.flatten(),grad2.flatten()))) * 2*np.real(solver.omega)**4/pi
            grad[:]=comm.bcast(grad)
        memory_report(str(verbose.count)+'-')
        return val
    return RHT_nlopt
