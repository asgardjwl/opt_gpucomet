import numpy as npa
import autograd.numpy as np
from autograd import make_vjp
from scipy.sparse import csr_matrix as csr
from functools import partial
from autograd.extend import defvjp,primitive
from gce import verbose

@primitive
def f_mul(x,D):
    return D.dot(x)

def grad_fmul(argnum,ans,x,D):
    if argnum == 0:
        return lambda g: D.transpose().dot(g)
defvjp(f_mul, partial(grad_fmul,0))

class Filter:
    def __init__(self,Mx=10,My=10,bproj=0.0,R=0,alpha=1.0,xsym=0,ysym=0,eta=0.5,order=1):
        self.Mx = Mx
        self.My = My
        self.bproj = bproj
        self.eta = eta
        self.R = R
        self.alpha = alpha
        self.xsym = xsym
        self.ysym = ysym
        self.order = order

        # density filter (a matrix) remains the same, so only generated once
        self.Df=self.f_density()
        
    # density filter for smoothing
    def f_symmetry(self,dof):
        Mx = self.Mx
        My = self.My
        
        if self.xsym == 1:
            My = My//2
        if self.ysym == 1:
            Mx = Mx//2
        df = np.reshape(dof,(Mx,My))
        if self.xsym == 1:
            df = np.hstack((df,np.fliplr(df)))
        if self.ysym == 1:
            df = np.vstack((df,np.flipud(df)))
        return df.flatten()
    
    def f_density(self):
        Mx = self.Mx
        My = self.My
        
        if self.xsym == 1 and self.order == 2:
            My = My//2
        if self.ysym == 1 and self.order == 2:
            Mx = Mx//2
            
        dim = Mx*My
        
        n0 = 2*self.R+1
        indptr = [0]
        indices = []
        data = []

        ctrlt = 0
        for i in range(dim):
            xi = int(i/My)
            yi = i%My
            norm = 0.0
            ctrl = 0
            dtmp = []
            
            for j in range(n0**2):
                xj = int(j/n0)+xi-self.R
                yj = j%n0+yi-self.R

                if xj<0 or xj>=Mx or yj<0 or yj>=My:
                    continue

                ctrl += 1
                indices.append(xj*My+yj)
                weight = np.exp(-1.0*((xi-xj)**2+(yi-yj)**2)/self.alpha**2)
                norm += weight
                dtmp.append(weight)

            for j in range(ctrl):
                dtmp[j] /= norm

            data += dtmp
            ctrlt += ctrl
            indptr.append(ctrlt)

        D = csr((data, indices, indptr), shape=(dim, dim))
        return D

    # projection filter for binarization
    def f_proj(self,dof):
        eta = self.eta
        bproj = self.bproj
        dofnew = np.where(dof<=eta,eta*(np.exp(-bproj*(1-dof/eta))-(1-dof/eta)*np.exp(-bproj)),(1-eta)*(1-np.exp(-bproj*(dof-eta)/(1-eta)) + (dof - eta)/(1-eta) * np.exp(-bproj)) + eta)
        return dofnew
        
    def new_dof(self,dof):
        if self.order == 1:
            if self.R>0:
                fun = lambda x: self.f_proj(f_mul(self.f_symmetry(x),self.Df))
            else:
                fun = lambda x: self.f_proj(self.f_symmetry(x))
        elif self.order == 2:
            if self.R>0:
                fun = lambda x: self.f_symmetry(self.f_proj(f_mul(x.flatten(),self.Df)))
            else:
                fun = lambda x: self.f_symmetry(self.f_proj(x.flatten()))
        return fun(dof)
            
    def new_object(self,old_object):
        def n_object(dof,grad):
            # export the original dof
            if verbose.export_in_object is 1 and verbose.count%verbose.outputbase is 0  and (self.bproj>0. or self.R>0 or self.xsym == 1 or self.ysym == 1):
                np.savetxt(verbose.filename+'dofinit'+str(verbose.count)+'.txt', dof)

            if self.order == 1:
                if self.R>0:
                    fun = lambda x: self.f_proj(f_mul(self.f_symmetry(x),self.Df))
                else:
                    fun = lambda x: self.f_proj(self.f_symmetry(x))
            elif self.order == 2:
                if self.R>0:
                    fun = lambda x: self.f_symmetry(self.f_proj(f_mul(x.flatten(),self.Df)))
                else:
                    fun = lambda x: self.f_symmetry(self.f_proj(x.flatten()))

            if (grad.size>0) and verbose.outputfield==0:
                vjp,dofnew = make_vjp(fun)(dof)
                grad0 = np.zeros(dofnew.shape)
                val = old_object(dofnew,grad0)
                grad[:] = vjp(grad0)
            else:
                dofnew = fun(dof)
                val = old_object(dofnew,grad)
            return val
        return n_object
