import numpy as np

def ellipsoid2dof_2d(dof,N,hxyzL,MxyzL,hole=1,xsym=0,ysym=0):
    '''
    dof = [x1,y1,rx,ry, ...]
    '''
    x0=np.linspace(1, MxyzL[0], MxyzL[0])*hxyzL[0]
    y0=np.linspace(1, MxyzL[1], MxyzL[1])*hxyzL[1]

    x,y = np.meshgrid(x0,y0,indexing = 'ij')
    egrid = np.ones_like(x)*hole
        
    for i in range(N):
        isin = ((x-dof[4*i])/dof[4*i+2])**2+((y-dof[4*i+1])/dof[4*i+3])**2-1
        egrid[isin<=0] = (1-hole)

    if xsym == 1:
        egrid = np.concatenate((egrid,np.flip(egrid,axis=0)),axis=0)
    if ysym == 1:
        egrid = np.concatenate((egrid,np.flip(egrid,axis=1)),axis=1)
        
    return egrid.flatten()

def ellipsoid2dof(dof,N,hxyzL,MxyzL,hole=1,xsym=0,ysym=0,zsym=0):
    '''
    dof = [x1,y1,z1,rx,ry,rz, ...]
    '''
    x0=np.linspace(1, MxyzL[0], MxyzL[0])*hxyzL[0]
    y0=np.linspace(1, MxyzL[1], MxyzL[1])*hxyzL[1]
    z0=np.linspace(1, MxyzL[2], MxyzL[2])*hxyzL[2]

    x,y,z = np.meshgrid(x0,y0,z0,indexing = 'ij')
    egrid = np.ones_like(x)*hole
        
    for i in range(N):
        isin = ((x-dof[6*i])/dof[6*i+3])**2+((y-dof[6*i+1])/dof[6*i+4])**2+((z-dof[6*i+2])/dof[6*i+5])**2-1
        egrid[isin<=0] = (1-hole)

    if xsym == 1:
        egrid = np.concatenate((egrid,np.flip(egrid,axis=0)),axis=0)
    if ysym == 1:
        egrid = np.concatenate((egrid,np.flip(egrid,axis=1)),axis=1)
    if zsym == 1:
        egrid = np.concatenate((egrid,np.flip(egrid,axis=2)),axis=2)        
        
    return egrid.flatten()
