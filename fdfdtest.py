import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import write_results, PML, interp, initialize, Multigrid
from lib.solver_mul import Solver
from lib.solver_mulgrid import Solver_mul
from lib.gce_mul.grid import Grid
from lib.gce_mul import verbose, shape_obj
from lib.gce_mul.space import initialize_space

dtype=np.complex128
#------------- passing argument---------------#

verbose.v=1
verbose.solverbase=10000
verbose.filename='test'
verbose.init=10
verbose.init_type='zero'
# ----------assembling parameters------------#
    
omega = dtype(2.0*2*pi)
shape = (650,400,120)
res = (0.01,0.01,0.01)
gridx = 1
gridy = 1
gridz = 2

shape2 = (shape[0]/gridx,shape[1]/gridy,shape[2]/gridz)
res2 = (res[0]*gridx,res[1]*gridy,res[2]*gridz)

m=4.0
R=16.0
pmlx=PML(10,omega,shape[0],res[0],m,R)
pmly=PML(10,omega,shape[1],res[1],m,R)
pmlz=PML(10,omega,shape[2],res[2],m,R)

pml_p=[pmlx.sp*res[0],pmly.sp*res[1],pmlz.sp*res[2]]
pml_d=[pmlx.sd*res[0],pmly.sd*res[1],pmlz.sd*res[2]]

pmlx2=PML(10/gridx,omega,shape2[0],res2[0],m,R)
pmly2=PML(10/gridy,omega,shape2[1],res2[1],m,R)
pmlz2=PML(10/gridz,omega,shape2[2],res2[2],m,R)

pml_p2=[pmlx2.sp*res2[0],pmly2.sp*res2[1],pmlz2.sp*res2[2]]
pml_d2=[pmlx2.sd*res2[0],pmly2.sd*res2[1],pmlz2.sd*res2[2]]
mul=Multigrid(shape,gridx,gridy,gridz)
if comm.rank == 0:
    print "The size of the problem is",shape
    # current source
    j=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)]);
    j[2][shape[0]/2-10:shape[0]/2+10,shape[1]/2-10:shape[1]/2+10,shape[2]/2-10:shape[2]/2+10]=np.ones((20,20,20)) #electric current
    #j[2][shape[0]/2,shape[1]/2,shape[2]/2]=1.0/res[0]/res[1] #electric current

    ep1 = np.ones(shape)
    #A = interp(shape[0]/2,shape[1]/2,shape[2]/2,1,shape[0],shape[1],shape[2],shape[0]/4,shape[1]/4,shape[2]/4)
    A = interp(400,300,20,1,shape[0],shape[1],shape[2],125,50,50)
    dof=np.ones([400,300]).astype(np.float)
    #dof=np.ones([shape[0]/2,shape[1]/2]).astype(np.float)
    A.matA(dof,ep1,10)

    # isotropic
    ep=dtype([ep1,ep1,ep1])
    ep2=mul.Restriction_linear(ep)

    # initial solution
    x=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    b=[-1j*omega*np.copy(z) for z in j] #b=-i omega j
else:
    ep=[None] * 3
    ep2=[None] * 3
    j=[None] * 3
    b=[None] * 3
    x = [None] * 3
#-------opertors----------#
#initiliaze GPU
shape_1=shape_obj.shape_obj(shape)
shape_2=shape_obj.shape_obj(shape2)
initialize_space()

solver1 = Solver(shape_1, 1e-3, 40000, pml_p, pml_d, omega)
solver2 = Solver(shape_2, 1e-3, 60000, pml_p2, pml_d2, omega)
solver1.update_ep(ep)
solver2.update_ep(ep2)

solver_m = Solver_mul(solver1,solver2, mul,tol_init=0.7,maxit_add=20000,maxmul=7)
# u = solver_m.coarse(b)
# if comm.rank == 0:
#     write_results('x2.h5',u)

if comm.rank == 0:
    b=solver1.pre_cond(b)
    print "------************ testing multigrid *************-----------"
xg = [Grid(dtype(np.copy(f)), shape_1, x_overlap=1) for f in x]
t1 = time.time()
xg, err, success = solver_m.Multisolve(b,xg)
t2 = time.time()

if comm.rank == 0:
    print "------------- time for multigrid = ",t2-t1
    print "------************ finishing testing multigrid *************-----------"
    print ""
    print ""
    print "------************ bicg alone *************-----------"
xg = [Grid(dtype(np.copy(f)), shape_1, x_overlap=1) for f in x]
solver1.max_iters = 140000
xg, err, success = solver1.Multisolve(b, xg)

# e2=[E.get() for E in xg2]
# if comm.rank == 0:
#     e2=solver2.post_cond(e2)
#     write_results('x2.h5',e2)
    
#     x=mul.Prolongation_linear(e2)
#     write_results('xp.h5',x)
    
#     x=solver1.pre_cond(x)

# xg = [Grid(dtype(f), shape_1, x_overlap=1) for f in x]    
# xg, err, success = solver1.Multisolve(b, xg)    
# e=[E.get() for E in xg]
# if comm.rank == 0:
#     e=solver1.post_cond(e)
#     write_results('x.h5',e)


# initialize(x, 0, verbose.init, verbose.init_type, shape, dtype)
# xg = [Grid(dtype(f), shape_1, x_overlap=1) for f in x]    
# xg, err, success = solver1.Multisolve(b, xg)        
