import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import write_results
from lib.tool import PML
from lib.tool import interp
from lib.tool import initialize
from lib.tool import generateE
from lib.solver import Solver
#from lib.solver_bloch import Solverb
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib.operators_bloch import M_step
from lib.operators_bloch import M_step_fun

from lib.operators import M_step as M0_step
#from lib.operators import MT_step as M0_step

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-Job', action="store", type=int, default=0)
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-6)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init', action="store", type=int, default=10)
parser.add_argument('-init_type', action="store", type=str, default='zero')
# temporal
parser.add_argument('-freq', action="store", type=float, default=1.0)
parser.add_argument('-Qabs', action="store", type=float, default=1e16)
parser.add_argument('-xsi', action="store", type=float, default=0.0)
# geometry
parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mx', action="store", type=int, default=25)
parser.add_argument('-My', action="store", type=int, default=25)
parser.add_argument('-Mz', action="store", type=int, default=20)
parser.add_argument('-Mzslab', action="store", type=int, default=1)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff', action="store", type=float, default=11.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='one')
# bloch
parser.add_argument('-kx', action="store", type=float, default=0.2)
parser.add_argument('-ky', action="store", type=float, default=0.0)
parser.add_argument('-kz', action="store", type=float, default=0.0)
# PEC
parser.add_argument('-PECx', action="store", type=int, default=0)
parser.add_argument('-PECy', action="store", type=int, default=0)
parser.add_argument('-PECz', action="store", type=int, default=0)
# current source
parser.add_argument('-Jamp', action="store", type=float, default=1.0)
parser.add_argument('-Jdir', action="store", type=int, default=2)
parser.add_argument('-cx', action="store", type=int, default=37)
parser.add_argument('-cy', action="store", type=int, default=37)
parser.add_argument('-cz', action="store", type=int, default=35)

r, unknown = parser.parse_known_args(sys.argv[1:])
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.init=r.init
verbose.init_type=r.init_type
# ----------assembling parameters------------#
if r.Qabs>1e10:
    r.Qabs=float('inf')
    
omega = dtype(r.freq*2*pi) - 1j*dtype(r.freq*2*pi/2/r.Qabs)
shape = (r.Nx,r.Ny,r.Nz)

m=4.0
R=16.0
pmlx=PML(r.Npmlx,omega,r.Nx,r.hx,m,R)
pmly=PML(r.Npmly,omega,r.Ny,r.hy,m,R)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz,m,R)

pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]
ndof=r.Mx*r.My
# pml
if comm.rank == 0:
    print "The size of the problem is",shape
    # current source
    j=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)]);
    j[r.Jdir][r.cx,r.cy,r.cz]=r.Jamp/r.hx/r.hy #electric current

    # epsilon profile
    if r.epstype == 'one':
        dof=np.ones([r.Mx,r.My]).astype(np.float)
    elif r.epstype == 'vac':
        dof=np.zeros([r.Mx,r.My]).astype(np.float)
    elif r.epstype == 'rand':
        dof=np.random.randn(r.Mx,r.My).astype(np.float)
    elif r.epstype == 'file':
        f = open(r.epsfile, 'r')
        dof = np.loadtxt(f).reshape(r.Mx,r.My)

    epsBkg = np.ones(shape)*r.epsbkg
    Mx0= (r.Nx-r.Mx)/2
    My0= (r.Ny-r.My)/2
    Mz0= (r.Nz-r.Mz)/2
    A = interp(r.Mx,r.My,r.Mz,r.Mzslab,r.Nx,r.Ny,r.Nz,Mx0,My0,Mz0)

    ep1 = np.copy(epsBkg)
    A.matA(dof,ep1,r.epsdiff)

    f=h5py.File('epsF.h5','w')
    f.create_dataset('data',data=ep1)
    f.close()

    # isotropic
    ep=dtype([ep1,ep1,ep1])

    # initial solution
    x=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    b=[-1j*omega*np.copy(z) for z in j] #b=-i omega j
        
else:
    ep=[None] * 3
    j=[None] * 3
    x=[None] * 3
    b=[None] * 3
    
initialize(x, 0, verbose.init, verbose.init_type, shape, dtype)
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)

PEC = [r.PECx,r.PECy,r.PECz]
job = r.Job

if job == 0:
    # test solver
    solver1 = Solver(shape, r.tol, r.maxit, pml_p, pml_d, omega,xsi=r.xsi)
    solver1.update_ep(ep)
    if comm.rank==0:
        b=solver1.pre_cond(b)
    # GPU vectors
    xg = [Grid(dtype(f), x_overlap=1) for f in x]
    #-------solve-----------#
    xg, err, success = solver1.Multisolve(b, xg)

    e=[E.get() for E in xg]
    if comm.Get_rank()==0:
        e=solver1.post_cond(e)
        write_results(r.name+'E.h5',e)
        
# elif job == 1:
#     # test solver
#     solver1 = Solverb(shape, r.tol, r.maxit, pml_p, pml_d, omega,r.hx,r.hy,r.hz,r.kx,r.ky,r.kz, PEC)
#     solver1.update_ep(ep)
#     if comm.rank==0:
#         write_results(r.name+'b.h5',b)
#         #b=solver1.pre_cond(b)
#     # GPU vectors
#     xg = [Grid(dtype(f), x_overlap=1) for f in x]
#     #-------solve-----------#
#     xg, err, success = solver1.Multisolve(b, xg)

#     e=[E.get() for E in xg]
#     if comm.Get_rank()==0:
#         #e=solver1.post_cond(e)
#         write_results(r.name+'E.h5',e)
        
elif job == 2:
    # test M_operator
    epw2 = [None]*3
    muinv=[None]*3
    xt=[None]*3
    yt=[None]*3
    if comm.rank==0:
        muinv = [np.ones(shape),np.ones(shape),np.ones(shape)]
        epw2 = [omega**2 * f for f in ep]
        xt = [generateE(shape),generateE(shape),generateE(shape)];
        yt = [generateE(shape),generateE(shape),generateE(shape)];
        
    #M=M0_step(shape,pml_p,pml_d,epw2,muinv,dtype)
    #MT=MT_step(shape,pml_p,pml_d,epw2,muinv,dtype)
    M_fun = M_step_fun(shape,dtype,transpose=0)
    M = M_step(M_fun,shape,pml_p,pml_d,epw2,muinv,dtype,bc)
    MT_fun = M_step_fun(shape,dtype,transpose=1)
    MT = M_step(MT_fun,shape,pml_p,pml_d,epw2,muinv,dtype,bc)
    # step 1: x^T M y, both x, y are random vectors
    ## v = M y
    ytg=[Grid(dtype(np.copy(f)), x_overlap=1) for f in yt]
    v = [Grid(dtype, x_overlap=1) for k in range(3)]
    v = M(ytg,v)
    vc=[E.get() for E in v]
    if comm.Get_rank()==0:
        val1=0.0-0.0j
        for i in range(3):
            val1 += np.vdot(np.conj(xt[i]),vc[i])
        print "x^TMy=",val1

    # step 2: y^T M x
    ## v = M x
    xtg=[Grid(dtype(np.copy(f)), x_overlap=1) for f in xt]
    v = [Grid(dtype, x_overlap=1) for k in range(3)]
    v = M(xtg,v)
    vc=[E.get() for E in v]
    if comm.Get_rank()==0:
        val2=0.0-0.0j
        for i in range(3):
            val2 += np.vdot(np.conj(yt[i]),vc[i])
        print "without transpose y^TMx=",val2
        print "rel error=",abs(val1-val2)/abs(val1)

    # step 3: y^T M^T x
    ## v = M x
    xtg=[Grid(dtype(np.copy(f)), x_overlap=1) for f in xt]
    v = [Grid(dtype, x_overlap=1) for k in range(3)]
    v = MT(xtg,v)
    vc=[E.get() for E in v]
    if comm.Get_rank()==0:
        val2=0.0-0.0j
        for i in range(3):
            val2 += np.vdot(np.conj(yt[i]),vc[i])
        print "with transpose y^TMx=",val2
        print "rel error=",abs(val1-val2)/abs(val1)

elif job == 3:
    # first solve
    solver1 = Solver(shape, r.tol, r.maxit, pml_d, pml_d, omega,bc)
    solver1.update_ep(ep)
    if comm.rank==0:
        # b[0]= dtype(generateE(shape))
        # b[1]= dtype(generateE(shape))
        # b[2]= dtype(generateE(shape))
        write_results(r.name+'b.h5',b)
        #b=solver1.pre_cond(b)
    # GPU vectors
    xg = [Grid(dtype(f), x_overlap=1) for f in x]
    #-------solve-----------#
    xg, err, success = solver1.Multisolve(b, xg)

    e=[E.get() for E in xg]
    if comm.Get_rank()==0:
        #e=solver1.post_cond(e)
        write_results(r.name+'x.h5',e)
    # then test M_operator
    epw2 = [None]*3
    muinv=[None]*3
    if comm.rank==0:
        muinv = [np.ones(shape),np.ones(shape),np.ones(shape)]
        epw2 = [omega**2 * f for f in ep]

    M_fun = M_step_fun(shape,dtype,transpose=0)
    M = M_step(M_fun,shape,pml_d,pml_d,epw2,muinv,dtype,bc)
    xg=[Grid(dtype(np.copy(f)), x_overlap=1) for f in e]
    v = [Grid(dtype, x_overlap=1) for k in range(3)]
    v = M(xg,v)
    vc=[E.get() for E in v]
    if comm.rank==0:
        write_results(r.name+'b2.h5',vc)
