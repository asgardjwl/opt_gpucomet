import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import interp, PML, write_results
from lib.tool import initialize,gradient_check
from lib.solver import Solver
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib import casimir

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-4)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init', action="store", type=int, default=10)
parser.add_argument('-init_type', action="store", type=str, default='zero')
# geometry
parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mx', action="store", type=int, default=25)
parser.add_argument('-My', action="store", type=int, default=25)
parser.add_argument('-Mz', action="store", type=int, default=20)
parser.add_argument('-Mx0', action="store", type=int, default=25)
parser.add_argument('-My0', action="store", type=int, default=25)
parser.add_argument('-Mz0', action="store", type=int, default=20)
parser.add_argument('-Mzslab', action="store", type=int, default=1)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff', action="store", type=float, default=11.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='one')
# current source
parser.add_argument('-Jamp', action="store", type=float, default=1.0)
parser.add_argument('-Jdir', action="store", type=int, default=2)
parser.add_argument('-cx', action="store", type=int, default=37)
parser.add_argument('-cy', action="store", type=int, default=37)
parser.add_argument('-cz', action="store", type=int, default=35)

r, unknown = parser.parse_known_args(sys.argv[1:])
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.init=r.init
verbose.init_type=r.init_type
# ----------assembling parameters------------#
omega = -1j*dtype(1.*2*pi)
shape = (r.Nx,r.Ny,r.Nz)

m=4.0
R=16.0
Npml = (r.Npmlx,r.Npmly,r.Npmlz)
hxyz = (r.hx,r.hy,r.hz)

pmlx=PML(r.Npmlx,omega,r.Nx,r.hx,m=m,R=R)
pmly=PML(r.Npmly,omega,r.Ny,r.hy,m=m,R=R)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz,m=m,R=R)

pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]

if r.Mzslab == 1:
    ndof=r.Mx*r.My
else:
    ndof=r.Mx*r.My*r.Mz
A = interp(r.Mx,r.My,r.Mz,r.Mzslab,r.Nx,r.Ny,r.Nz,r.Mx0,r.My0,r.Mz0)
ep=[None] * 3
x=[None] * 3
epsBkg=[None]
dof=[None]
if comm.rank == 0:
    print "The size of the problem is",shape

    # epsilon profile
    if r.epstype == 'one':
        dof=np.ones(ndof).astype(np.float)
    elif r.epstype == 'vac':
        dof=np.zeros(ndof).astype(np.float)
    elif r.epstype == 'rand':
        dof=np.random.random(ndof).astype(np.float)
    elif r.epstype == 'file':
        f = open(r.epsfile, 'r')
        dof = np.loadtxt(f)
        
    if r.Mzslab == 1:
        dof = dof.reshape(r.Mx,r.My)
    else:
        dof = dof.reshape(r.Mx,r.My,r.Mz)

    epsBkg = np.ones(shape)*r.epsbkg

    ep1 = np.copy(epsBkg)
    A.matA(dof,ep1,r.epsdiff)

    # f=h5py.File('epsF.h5','w')
    # f.create_dataset('data',data=ep1)
    # f.close()

    # isotropic
    ep=dtype([ep1,ep1,ep1])

    # initial solution
    x=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])

dof = comm.bcast(dof)
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)
solver1 = Solver.build_pml(shape, r.tol, r.maxit, Npml, hxyz, omega,m=m,R=R)

Job = 2

if Job == 0:
    freq = 1e-2
    omega = -1j*freq*2*pi
    solver1.update_ep_omega(ep,omega)

    b = [None] * 3
    p = casimir.setup_source(solver1.shape,r.cx,r.cy,r.cz,1,r.hx*r.hy*r.hz)
    if comm.rank == 0:
        b = solver1.pre_cond(p)
        initialize(x, verbose.count, verbose.init, verbose.init_type, solver1.shape, dtype)
    # solve
    xg = [Grid(dtype(f), x_overlap=1) for f in x]
    xg, err, success = solver1.Multisolve(b, xg)
        
    field=[E.get() for E in xg]

    if comm.rank==0:
        field=solver1.post_cond(field)
        write_results('x.h5',field)
        
elif Job == 1:
    ifreq_list = np.logspace(-2,0.,10)
    #ifreq_list = [1e-2]
    alphaxyz = [1.,1.,1.]
    fun = casimir.forceZintegrate(ifreq_list,solver1,A,epsBkg,r.epsdiff,x,alphaxyz,r.cx,r.cy,r.cz,Dmethod='center',step=1,stype=None)

    grad = np.array([])
    val, val_list = fun(dof,grad)
    print val, val_list

elif Job == 2:
    ifreq_list = np.array([0.2,0.3])
    alphaxyz = [0.5,-1.,2.]
    fun = casimir.forceZintegrate(ifreq_list,solver1,A,epsBkg,r.epsdiff,x,alphaxyz,r.cx,r.cy,r.cz,Dmethod='center',step=1,stype=None,Qabs=10.)

    grad=np.ones(ndof)
    gradient_check(fun, dof.flatten(), grad, index=50, Ntotal=10, start=0.0, dx=1e-2)
