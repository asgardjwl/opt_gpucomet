#!/bin/bash
#SBATCH --job-name=test
#SBATCH --output=test.txt
#SBATCH -p gpu-shared
#SBATCH --gres=gpu:1
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=00:30:00
#SBATCH --mem=24000
#SBATCH --error=test.err

export PATH="/home/asgard/miniconda2/bin:$PATH"
module load cuda openmpi_ib

prop=example.py
np=1
Job=0
freq=1
Qabs=1e16

Nx=100
Ny=100
Nz=100
Mx=50
My=50
Mz=50
Mzslab=1

hx=0.02
hy=0.02
hz=0.02

Npmlx=10
Npmly=10
Npmlz=10

epsbkg=1
epsdiff=6
epstype='vac'
epsfile='c.txt'

Jamp=1.0
Jdir=2
cx=$(($Nx/2))
cy=$(($Ny/2))
cz=$(($Nz/2))

# solver
maxit=10000
tol=1e-5
verbose=1
init=2
init_type='rand'

solverbase=500

mpiexec -n $np python $prop -Job $Job -freq $freq -Qabs $Qabs -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mzslab $Mzslab -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg $epsbkg -epsdiff $epsdiff -epstype $epstype -epsfile $epsfile -Jamp $Jamp -Jdir $Jdir -cx $cx -cy $cy -cz $cz -maxit $maxit -tol $tol -verbose $verbose -solverbase $solverbase -init $init -init_type $init_type
