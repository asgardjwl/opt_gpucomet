import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.erosion import interp_erosion as interp
from lib.tool import write_results, PML,  gradient_check
from lib.ldos_erosion import coupler_total, coupler_normalize
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib.solver import Solver
from lib.filter_grad import Filter

#sys.path.append('/home/asgard/nlopt-python/lib/python2.7/site-packages/')
sys.path.append('/home/weiliang/nlopt-2.5.0/lib/python2.7/site-packages/')
import nlopt

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-6)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init', action="store", type=int, default=10)
parser.add_argument('-init_type', action="store", type=str, default='zero')
parser.add_argument('-outputfield', action="store", type=int, default=0)
# erosion
parser.add_argument('-angle', action="store", type=float, default=0.0)
parser.add_argument('-scale', action="store", type=float, default=100.0)
parser.add_argument('-erobproj', action="store", type=float, default=0.0)
parser.add_argument('-Yeeshift', action="store", type=int, default=1)
parser.add_argument('-mode', action="store", type=str, default='dilation')
# filter
parser.add_argument('-bproj', action="store", type=float, default=0.0)
parser.add_argument('-eta', action="store", type=float, default=0.5)
parser.add_argument('-R', action="store", type=int, default=0)
parser.add_argument('-alpha', action="store", type=float, default=1.0)
# Job
parser.add_argument('-Job', action="store", type=int, default=1)
parser.add_argument('-outputbase', action="store", type=int, default=2)
parser.add_argument('-export_in_object', action="store", type=int, default=1)
parser.add_argument('-maxeval', action="store", type=int, default=5000)
# temporal
parser.add_argument('-freq1', action="store", type=float, default=1.0)
parser.add_argument('-freq2', action="store", type=float, default=0.5)
parser.add_argument('-Qabs', action="store", type=float, default=200)
# geometry
parser.add_argument('-order', action="store", type=int, default=1)
parser.add_argument('-xsym', action="store", type=int, default=0)
parser.add_argument('-ysym', action="store", type=int, default=0)
parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mx', action="store", type=int, default=25)
parser.add_argument('-My', action="store", type=int, default=25)
parser.add_argument('-Mz', action="store", type=int, default=20)
parser.add_argument('-Mx0', action="store", type=int, default=25)
parser.add_argument('-My0', action="store", type=int, default=25)
parser.add_argument('-Mz0', action="store", type=int, default=20)
parser.add_argument('-Mzsub0', action="store", type=int, default=-1)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# dielectric
parser.add_argument('-epsbkg1', action="store", type=float, default=1.0)
parser.add_argument('-epssub1', action="store", type=float, default=1.0)
parser.add_argument('-epssub2_1', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff1', action="store", type=float, default=11.0)
parser.add_argument('-epsbkg2', action="store", type=float, default=1.0)
parser.add_argument('-epssub2', action="store", type=float, default=1.0)
parser.add_argument('-epssub2_2', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff2', action="store", type=float, default=11.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='vac')

# current source
parser.add_argument('-Jamp', action="store", type=float, default=1.0)
parser.add_argument('-Jdir', action="store", type=int, default=2)
parser.add_argument('-cx', action="store", type=int, default=37)
parser.add_argument('-cy', action="store", type=int, default=37)
parser.add_argument('-cz', action="store", type=int, default=35)

# SHG
parser.add_argument('-p1', action="store", type=int, default=0)
parser.add_argument('-p2', action="store", type=int, default=2)
parser.add_argument('-powerindex', action="store", type=float, default=0)
parser.add_argument('-lap_x0', action="store", type=int, default=37)
parser.add_argument('-lap_y0', action="store", type=int, default=37)
parser.add_argument('-lap_z0', action="store", type=int, default=35)
parser.add_argument('-lap_tx', action="store", type=int, default=1)
parser.add_argument('-lap_ty', action="store", type=int, default=1)
parser.add_argument('-lap_tz', action="store", type=int, default=1)
parser.add_argument('-abs_x0', action="store", type=int, default=37)
parser.add_argument('-abs_y0', action="store", type=int, default=37)
parser.add_argument('-abs_z0', action="store", type=int, default=35)
parser.add_argument('-abs_tx', action="store", type=int, default=1)
parser.add_argument('-abs_ty', action="store", type=int, default=1)
parser.add_argument('-abs_tz', action="store", type=int, default=1)

# wvg
parser.add_argument('-with_wvg', action="store", type=int, default=0)
parser.add_argument('-wvg_x0', action="store", type=int, default=37)
parser.add_argument('-wvg_y0', action="store", type=int, default=37)
parser.add_argument('-wvg_z0', action="store", type=int, default=35)
parser.add_argument('-wvg_tx', action="store", type=int, default=1)
parser.add_argument('-wvg_ty', action="store", type=int, default=1)
parser.add_argument('-wvg_tz', action="store", type=int, default=1)

parser.add_argument('-with_wvg2', action="store", type=int, default=0)
parser.add_argument('-wvg2_x0', action="store", type=int, default=37)
parser.add_argument('-wvg2_y0', action="store", type=int, default=37)
parser.add_argument('-wvg2_z0', action="store", type=int, default=35)
parser.add_argument('-wvg2_tx', action="store", type=int, default=1)
parser.add_argument('-wvg2_ty', action="store", type=int, default=1)
parser.add_argument('-wvg2_tz', action="store", type=int, default=1)

r, unknown = parser.parse_known_args(sys.argv[1:])
if comm.rank==0:
    for arg in vars(r):
        print arg," is ",getattr(r,arg)
        
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.outputbase=r.outputbase
verbose.export_in_object=r.export_in_object
verbose.init=r.init
verbose.init_type=r.init_type
verbose.outputfield=r.outputfield
verbose.lap_x0=r.lap_x0
verbose.lap_y0=r.lap_y0
verbose.lap_z0=r.lap_z0
verbose.lap_tx=r.lap_tx
verbose.lap_ty=r.lap_ty
verbose.lap_tz=r.lap_tz
# ----------assembling parameters------------#
hxyz=r.hx*r.hy*r.hz
if r.Qabs>1e10:
    r.Qabs=float('inf')
    
omega1 = dtype(r.freq1*2*pi) - 1j*dtype(r.freq1*2*pi/2/r.Qabs)
omega2 = dtype(r.freq2*2*pi) - 1j*dtype(r.freq2*2*pi/2/r.Qabs)
shape = (r.Nx,r.Ny,r.Nz)
# pml
pmlx1=PML(r.Npmlx,omega1,r.Nx,r.hx)
pmly1=PML(r.Npmly,omega1,r.Ny,r.hy)
pmlz1=PML(r.Npmlz,omega1,r.Nz,r.hz)

pmlx2=PML(r.Npmlx,omega2,r.Nx,r.hx)
pmly2=PML(r.Npmly,omega2,r.Ny,r.hy)
pmlz2=PML(r.Npmlz,omega2,r.Nz,r.hz)

pml_p1=[pmlx1.sp*r.hx,pmly1.sp*r.hy,pmlz1.sp*r.hz]
pml_d1=[pmlx1.sd*r.hx,pmly1.sd*r.hy,pmlz1.sd*r.hz]

pml_p2=[pmlx2.sp*r.hx,pmly2.sp*r.hy,pmlz2.sp*r.hz]
pml_d2=[pmlx2.sd*r.hx,pmly2.sd*r.hy,pmlz2.sd*r.hz]

#interp matrix
Mxo = r.Mx//(r.ysym+1)
Myo = r.My//(r.xsym+1)
if r.Yeeshift == 0:
    Yeeshift = False
else:
    Yeeshift = True
if r.mode == 'dilation':
    r.scale = -r.scale
    
A = interp(r.Mx,r.My,r.Mz,r.Nx,r.Ny,r.Nz,r.Mx0,r.My0,r.Mz0,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=Yeeshift,mode=r.mode)
FT = Filter(Mx=r.Mx,My=r.My,bproj=r.bproj,R=r.R,alpha=r.alpha,xsym=r.xsym,ysym=r.ysym,eta=r.eta,order=r.order)

if r.epstype == 'one':
    dof=np.ones([Mxo,Myo]).astype(np.float)
elif r.epstype == 'rand':
    dof=np.random.random([Mxo,Myo]).astype(np.float)
elif r.epstype == 'file':
    f = open(r.epsfile, 'r')
    dof = np.loadtxt(f).reshape((Mxo,Myo))
elif r.epstype == 'vac':
    dof=np.zeros([Mxo,Myo]).astype(np.float)
    dof[Mxo//2,Myo//2]=1.0
elif r.epstype == 'vacrand':
    dof=1e-2*np.random.random([Mxo,Myo]).astype(np.float)

if comm.rank == 0:
    print "The size of the problem is",shape
    # current source
    j=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)]);
    if r.with_wvg ==1:
        j[r.Jdir][r.wvg_x0:r.wvg_x0+r.wvg_tx,r.cy,r.wvg_z0:r.wvg_z0+r.wvg_tz]=r.Jamp/r.hx/r.hy
    else:
        j[r.Jdir][r.cx,r.cy,r.cz]=r.Jamp/r.hx/r.hy #electric current

    # epsilon profile
    epsBkg1 = np.ones(shape)*r.epsbkg1
    epsBkg2 = np.ones(shape)*r.epsbkg2
    eps0 = [dtype(np.zeros(shape)) for i in range(3)]

    if r.Mzsub0<0:
        epsBkg1[:,:,:A.Mz0] = r.epssub1
        epsBkg2[:,:,:A.Mz0] = r.epssub2
    else:
        epsBkg1[:,:,r.Mzsub0:A.Mz0] = r.epssub1
        epsBkg2[:,:,r.Mzsub0:A.Mz0] = r.epssub2
        if r.epssub2_1>1:
            epsBkg1[:,:,:r.Mzsub0] = r.epssub2_1
            epsBkg2[:,:,:r.Mzsub0] = r.epssub2_2
        else:
            eps0[0][:,:,r.Mzsub0:A.Mz0] = 1.0
            eps0[1][:,:,r.Mzsub0:A.Mz0] = 1.0
            eps0[2][:,:,r.Mzsub0:A.Mz0] = 1.0
        
    ep1 = [np.copy(epsBkg1) for i in range(3)]
    epsBkg1 = [np.copy(f) for f in ep1]
    ep2 = [np.copy(epsBkg2) for i in range(3)]
    epsBkg2 = [np.copy(f) for f in ep2]
    
    if r.with_wvg == 1:
        xadd = int(round(A.slope*r.wvg_tz))
        Awvg = interp(r.wvg_tx+2*xadd,r.wvg_ty,r.wvg_tz,r.Nx,r.Ny,r.Nz,r.wvg_x0-xadd,r.wvg_y0,r.wvg_z0,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=False,mode=r.mode)
        dofwvg = np.zeros([r.wvg_tx+2*xadd,r.wvg_ty])
        if xadd>0:
            dofwvg[xadd:-xadd,:]=1
        else:
            dofwvg[:,:]=1        
        _ = Awvg.matA(dofwvg,epsBkg1,r.epsdiff1)
        _ = Awvg.matA(dofwvg,epsBkg2,r.epsdiff2)
        
    if r.with_wvg2 == 1:
        xadd = int(round(A.slope*r.wvg2_tz))
        Awvg = interp(r.wvg2_tx+2*xadd,r.wvg2_ty,r.wvg2_tz,r.Nx,r.Ny,r.Nz,r.wvg2_x0-xadd,r.wvg2_y0,r.wvg2_z0,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=False,mode=r.mode)
        dofwvg = np.zeros([r.wvg2_tx+2*xadd,r.wvg2_ty])
        if xadd>0:
            dofwvg[xadd:-xadd,:]=1
        else:
            dofwvg[:,:]=1
        _ = Awvg.matA(dofwvg,epsBkg1,r.epsdiff1)
        _ = Awvg.matA(dofwvg,epsBkg2,r.epsdiff2)        

    dn = FT.new_dof(dof)
    ep1 = [np.copy(f) for f in epsBkg1]
    ep2 = [np.copy(f) for f in epsBkg2]    
    _ = A.matA(dn,ep1,r.epsdiff1)
    _ = A.matA(dn,ep2,r.epsdiff2)    

    f=h5py.File('epsF1.h5','w')
    f.create_dataset('data',data=ep1[0])
    f.close()
    
    f=h5py.File('epsF2.h5','w')
    f.create_dataset('data',data=ep2[0])
    f.close()    

    # initial solution
    x1=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    x2=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    u1=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    u2=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    u3=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    b=[-1j*omega1*np.copy(z) for z in j] #b=-i omega j

    # overlap vector
    lap = dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    lap[r.p2][r.lap_x0:r.lap_x0+r.lap_tx,r.lap_y0:r.lap_y0+r.lap_ty,r.lap_z0:r.lap_z0+r.lap_tz]=1

    # abs vector
    f1abs = dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    f1abs[r.p1][r.abs_x0:r.abs_x0+r.abs_tx,r.abs_y0:r.abs_y0+r.abs_ty,r.abs_z0:r.abs_z0+r.abs_tz]=1    

    # f=h5py.File('lap.h5','w')
    # f.create_dataset('data',data=np.real(lap[r.p2]))
    # f.close()

    # f=h5py.File('f1abs.h5','w')
    # f.create_dataset('data',data=np.real(f1abs[r.p1]))
    # f.close()    
        
else:
    lap=[None] * 3
    f1abs=[None] * 3
    j=[None] * 3
    x1=[None] * 3
    x2=[None] * 3
    u1=[None] * 3
    u2=[None] * 3
    u3=[None] * 3
    b=[None] * 3
    epsBkg1 = [None]*3
    epsBkg2 = [None]*3
    eps0=[None] *3
    
verbose.lap=lap
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)
ndofo=Mxo*Myo
ndof=r.Mx*r.My
# solver
if r.Job is -1:
    solver1 = Solver(shape, r.tol, r.maxit, pml_p1, pml_d1, omega1)
    solver2 = Solver(shape, r.tol, r.maxit, pml_p2, pml_d2, omega2)
else:
    solver1 = Solver(shape, r.tol, r.maxit, pml_d1, pml_d1, omega1)
    solver2 = Solver(shape, r.tol, r.maxit, pml_d2, pml_d2, omega2)
    
if comm.rank==0:
    b=solver1.pre_cond(b)
#-------solve-----------#
grad=np.zeros(ndofo)
if abs(r.powerindex) <= 0.01:
    if comm.rank == 0:
        print "objective without normalization"
    coupler_old=coupler_total(shape, b, x1, x2, u1, u2, solver1, solver2, epsBkg1 ,epsBkg2, r.epsdiff1, r.epsdiff2, A, omega1, omega2, r.p1, r.p2,eps0=eps0)
else:
    if comm.rank == 0:
        print "objective with normalization"
    coupler_old=coupler_normalize(shape, b, x1, x2, u1, u2, u3, solver1, solver2, epsBkg1 ,epsBkg2, r.epsdiff1, r.epsdiff2, A, lap, f1abs, r.p1, r.p2, r.powerindex,eps0=eps0)

coupler_nlopt = FT.new_object(coupler_old)

if r.Job is 0:
    grad=np.array([])
    val=coupler_nlopt(dof.flatten(),grad)
    print val
    
elif r.Job is -1:
    verbose.count=1
    gradient_check(coupler_nlopt, dof.flatten(), grad, index=1, Ntotal=20, start=0.0, dx=1e-3)
    
elif r.Job is 1:
    lb=np.ones(ndofo)*0.0
    ub=np.ones(ndofo)*1.0
    
    opt = nlopt.opt(nlopt.LD_MMA, ndofo)
    opt.set_lower_bounds(lb)
    opt.set_upper_bounds(ub)
    
    opt.set_maxeval(r.maxeval)

    opt.set_max_objective(coupler_nlopt)
    
    x = opt.optimize(dof.flatten())
    minf = opt.last_optimum_value()

