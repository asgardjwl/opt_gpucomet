d=0.1;
x0=1e4;
mx=[0.02 0.04 0.06 0.08 0.1 0.2 0.3 0.4 0.5];
mz=0.5;
A=mx.^2;
V=A*mz;

phi=[];
for ii=mx
    if ii == 0.2 || ii == 0.5||ii==0.4
        name_=sprintf('mx%g-my%g-mz%g-d%g-hx0.01-f1.0-tol1e-6-svdtol1e-6-m70.txt',ii,ii,mz,d);
    else
        name_=sprintf('mx%g-my%g-mz%g-d%g-hx0.01-f1.0-tol1e-6-svdtol1e-3-m50.txt',ii,ii,mz,d);
    end
    phi = [phi compute_RHTbound(name_,x0)];
end

mxd=linspace(mx(1),mx(end),1000);
Ad=mxd.^2;
Vd=Ad*mz;
y_plate=2/pi*Ad/8/pi/d^2.*log(1+0.25*x0);

%near
y_dipole1=2/pi*x0*Vd.^2/8/pi^2/d^6.*((1+x0*Vd.^2/16/pi^2/d^6).^-2+2*(1+x0*Vd.^2/4/pi^2/d^6).^-2);
%middle
d2=d+mz/3.5;
y_dipole2=2/pi*x0*Vd.^2/8/pi^2/d2^6.*((1+x0*Vd.^2/16/pi^2/d2^6).^-2+2*(1+x0*Vd.^2/4/pi^2/d2^6).^-2);

%loglog(A/d^2,phi./A,'r.-',Ad/d^2,y_dipole1./Ad,'k--',Ad/d^2,y_dipole2./Ad,'k-.',Ad/d^2,y_plate./Ad,'b--','LineWidth',2,'MarkerSize',25)

loglog(A/d^2,phi./A,'r.-',Ad/d^2,y_dipole2./Ad,'k-.',Ad/d^2,y_plate./Ad,'b--','LineWidth',2,'MarkerSize',25)
legend('compact','dipolar','planar')
axis([-inf inf 1e-2 1000])
xlabel('A (d^2)')
ylabel('\phi/A')
set(gca,'FontSize',15)
