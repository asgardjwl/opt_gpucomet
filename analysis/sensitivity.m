f1file = 'Mx150-My200-w0.3-Mz8-f1.0-n1-N10-tol1e-4-g0.5-w1.3.x0.h5';
eps1file = 'Mx150-My200-w0.3-Mz8-f1.0-n1-N10-tol1e-4-g0.5-w1.3.epsF.h5';
f2file='Mx150-My200-w0.3-Mz8-f2.0-n1-N10-tol1e-4-g0.5-w1.3.x0.h5';
eps2file='Mx150-My200-w0.3-Mz8-f2.0-n1-N10-tol1e-4-g0.5-w1.3.epsF.h5';
hx=0.02;

Mz0=36;
Mz=8;
Mx0=36;
My0=101;
Mx=150;
My=200;

Ex1=h5read(f1file,'/xr')+1i*h5read(f1file,'/xi');
Ey1=h5read(f1file,'/yr')+1i*h5read(f1file,'/yi');
Ez1=h5read(f1file,'/zr')+1i*h5read(f1file,'/zi');
E1=abs(Ex1).^2+abs(Ey1).^2+abs(Ez1).^2;

Ex2=h5read(f2file,'/xr')+1i*h5read(f2file,'/xi');
Ey2=h5read(f2file,'/yr')+1i*h5read(f2file,'/yi');
Ez2=h5read(f2file,'/zr')+1i*h5read(f2file,'/zi');
E2=abs(Ex2).^2+abs(Ey2).^2+abs(Ez2).^2;

eps1=h5read(eps1file,'/data');
eps2=h5read(eps2file,'/data');

[Nz,Ny,Nx]=size(E1);
e1z=sum(E1(Mz0:Mz0+Mz-1,:,:));e1z=reshape(e1z,Ny,Nx)/max(e1z(:));
e2z=sum(E2(Mz0:Mz0+Mz-1,:,:));e2z=reshape(e2z,Ny,Nx)/max(e2z(:));

fom = (e1z*2 + e2z)/3;
fom = fom(My0:My0+My-1,Mx0:Mx0+Mx-1);

subplot(1,2,1)
epc=eps1(Mz0+1,My0:My0+My-1,Mx0:Mx0+Mx-1);
epc=reshape(epc,My,Mx);
imagesc(epc);shading flat;colorbar;axis tight equal

subplot(1,2,2)
imagesc(log10(fom));shading flat;colorbar;axis tight equal
caxis([-2,0])
