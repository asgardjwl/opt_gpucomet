%filename='mx0.1-my0.1-mz0.1-d0.1-f1.0-tol1e-6-svdtol1e-4-m50.txt';
%filename='mx0.04-my0.04-mz0.04-d0.1-f1.0-tol1e-6-svdtol1e-4-m50..txt';
%filename='mx0.5-my0.5-mz0.5-d0.1-f1.0-tol1e-6-svdtol1e-4-m80..txt';
filename='mx0.02-my0.02-mz0.1-d0.1-hx0.01-f1.0-tol1e-6-svdtol1e-3-m50.txt';

mx=0.02;
my=mx;
mz=0.1;
V=mx*my*mz;A=mx*my;
d=0.1;
x0=logspace(-8,10,1000);

x_dipole=sqrt(x0*V^2/d^6);
x_plate=sqrt(x0*A/d^2);
y_dipole=2/pi*x0*V^2/8/pi^2/d^6.*((1+x0*V^2/16/pi^2/d^6).^-2+2*(1+x0*V^2/4/pi^2/d^6).^-2);
y_plate=2/pi*A/8/pi/d^2.*log(1+0.25*x0);

sigma0 = load(filename);
sigma=sigma0.^2;
xmin=1/sigma(1)
xmax=1/sigma(end)

phi=zeros(size(x0));
for ii=1:length(x0)
    phi(ii)=2/pi*sum(x0(ii)*sigma./(1+x0(ii)*sigma).^2);
end
loglog(x_dipole,y_dipole,'k--',x_dipole,phi,'m','LineWidth',2);
%loglog(x_dipole,phi,'r','LineWidth',2);
%loglog(x_plate,y_plate,'k--',x_plate,phi,'r','LineWidth',2);
%loglog(x0,y_dipole*d^6/V^2,'k--',x0,phi*d^6/V^2,'b','LineWidth',2);