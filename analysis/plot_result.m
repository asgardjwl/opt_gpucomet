f1file = 'Mx150-My200-w0.3-Mz8-f1.0-n1-N10-tol1e-4-g0.5-w1.3.x0.h5';
eps1file = 'Mx150-My200-w0.3-Mz8-f1.0-n1-N10-tol1e-4-g0.5-w1.3.epsF.h5';
f2file='Mx150-My200-w0.3-Mz8-f2.0-n1-N10-tol1e-4-g0.5-w1.3.x0.h5';
eps2file='Mx150-My200-w0.3-Mz8-f2.0-n1-N10-tol1e-4-g0.5-w1.3.epsF.h5';

Ex1=h5read(f1file,'/xr')+1i*h5read(f1file,'/xi');
Ey1=h5read(f1file,'/yr')+1i*h5read(f1file,'/yi');
Ez1=h5read(f1file,'/zr')+1i*h5read(f1file,'/zi');

Ex2=h5read(f2file,'/xr')+1i*h5read(f2file,'/xi');
Ey2=h5read(f2file,'/yr')+1i*h5read(f2file,'/yi');
Ez2=h5read(f2file,'/zr')+1i*h5read(f2file,'/zi');

eps1=h5read(eps1file,'/data');
eps2=h5read(eps2file,'/data');
[Nz,Ny,Nx]=size(eps1);

% p=eps1;
% p=p(Nz/2,:,:);p=reshape(p,Ny,Nx);pcolor(p);shading flat;axis tight equal;
% colormap(flipud(gray));
% axis off

subplot(2,2,1)
p=real(Ex1);
p=p(Nz/2,:,:);p=reshape(p,Ny,Nx);pcolor(p);shading flat;colorbar;axis tight equal;
title('Re(Ex) at FF')
axis off

subplot(2,2,2)
p=imag(Ex1);
p=p(Nz/2,:,:);p=reshape(p,Ny,Nx);pcolor(p);shading flat;colorbar;axis tight equal;
title('Im(Ex) at FF')
axis off

subplot(2,2,3)
p=real(Ez2);
p=p(Nz/2,:,:);p=reshape(p,Ny,Nx);pcolor(p);shading flat;colorbar;axis tight equal;
title('Re(Ez) at SH')
axis off

subplot(2,2,4)
p=imag(Ez2);
p=p(Nz/2,:,:);p=reshape(p,Ny,Nx);pcolor(p);shading flat;colorbar;axis tight equal;
title('Im(Ez) at SH')
axis off