import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
from math import pi
import time

from lib.tool import write_results,memory_report, PML, interp, gradient_check
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib.solver import Solver
from lib.RHT import RHT_box, RHT_box_gradient
dtype=np.complex128

shape=(100,100,100)
hx=0.02
hy=0.02
hz=0.02
R=1
Mx=2*R
My=2*R
Mz=2*R
d=10
chi=2.0;
epsimag=1;

freq=1.0
omega = dtype(freq*2*pi)

pmlx=PML(10,omega,shape[0],hx)
pmly=PML(10,omega,shape[1],hy)
pmlz=PML(10,omega,shape[2],hz)
pml_p=[pmlx.sp*hx,pmly.sp*hy,pmlz.sp*hz]
pml_d=[pmlx.sd*hx,pmly.sd*hy,pmlz.sd*hz]

verbose.v=1
verbose.solverbase=1000
verbose.init_type='zero'
#initiliaze GPU
initialize_space(shape)
solver = Solver(shape, 1e-5, 70000, pml_p, pml_d, omega)

Aup = interp(Mx,My,Mz,0,shape[0],shape[1],shape[2],(shape[0]-Mx)/2,(shape[1]-My*2-d)/2,(shape[2]-Mz)/2,1)
Adown = interp(Mx,My,Mz,0,shape[0],shape[1],shape[2],(shape[0]-Mx)/2,(shape[1]-My*2-d)/2+My+d,(shape[2]-Mz)/2,1)
dof1 = np.ones((Mx,My,Mz))

# for ii in range(Mx):
#     for jj in range(My):
#         for kk in range(Mz):
#             if (ii-R)**2+(jj-R)**2+(kk-R)**2<=R**2:
#                 dof1[ii][jj][kk]=1.0
                
dof = np.concatenate((dof1.flatten(),dof1.flatten()))
grad=np.zeros_like(dof)
epsBkg=[]
if comm.rank == 0:
    epsBkg = np.ones(shape)
    
fun = RHT_box_gradient(shape,epsBkg,chi,epsimag,solver,Aup, Adown, 1e-3, 30)
gradient_check(fun, dof, grad, index=9, Ntotal=10, start=0.0, dx=1e-1)
#val = fun(dof,grad)
print val
