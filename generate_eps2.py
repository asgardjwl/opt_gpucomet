import h5py,sys
import numpy as np
import argparse
from scipy.interpolate import griddata

from lib.tool import write_results
from lib.erosion import interp_erosion, Erosion

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
parser.add_argument('-name', action="store", type=str, default='test')
# geometry
parser.add_argument('-testYee', action="store", type=int, default=-1)

parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mx', action="store", type=int, default=25)
parser.add_argument('-My', action="store", type=int, default=25)
parser.add_argument('-Mz', action="store", type=int, default=20)
parser.add_argument('-Mxo', action="store", type=int, default=25)
parser.add_argument('-Myo', action="store", type=int, default=25)
parser.add_argument('-Mzo', action="store", type=int, default=20)
parser.add_argument('-Mzsub0', action="store", type=int, default=-1)
parser.add_argument('-Mx0', action="store", type=int, default=-1)
parser.add_argument('-My0', action="store", type=int, default=-1)
parser.add_argument('-Mz0', action="store", type=int, default=-1)
parser.add_argument('-withtop', action="store", type=int, default=0)

# erosion
parser.add_argument('-angle', action="store", type=float, default=0.0)
parser.add_argument('-scale', action="store", type=float, default=100.0)
parser.add_argument('-erobproj', action="store", type=float, default=0.0)
parser.add_argument('-l1', action="store", type=int, default=2)
parser.add_argument('-l2', action="store", type=int, default=6)
# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epssub', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff', action="store", type=float, default=11.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
# wvg
parser.add_argument('-with_wvg', action="store", type=int, default=0)
parser.add_argument('-wvg_x0', action="store", type=int, default=37)
parser.add_argument('-wvg_y0', action="store", type=int, default=37)
parser.add_argument('-wvg_z0', action="store", type=int, default=35)
parser.add_argument('-wvg_tx', action="store", type=int, default=1)
parser.add_argument('-wvg_ty', action="store", type=int, default=1)
parser.add_argument('-wvg_tz', action="store", type=int, default=1)
parser.add_argument('-with_wvg2', action="store", type=int, default=0)
parser.add_argument('-wvg2_x0', action="store", type=int, default=37)
parser.add_argument('-wvg2_y0', action="store", type=int, default=37)
parser.add_argument('-wvg2_z0', action="store", type=int, default=35)
parser.add_argument('-wvg2_tx', action="store", type=int, default=1)
parser.add_argument('-wvg2_ty', action="store", type=int, default=1)
parser.add_argument('-wvg2_tz', action="store", type=int, default=1)

r, unknown = parser.parse_known_args(sys.argv[1:])
for arg in vars(r):
    print arg," is ",getattr(r,arg)
        
shape = (r.Nx,r.Ny,r.Nz)
#interp matrix
ndof=r.Mx*r.My
if r.Mx0<0:
    Mx0= (r.Nx-r.Mx)/2
else:
    Mx0=r.Mx0
if r.My0<0:
    My0= (r.Ny-r.My)/2
else:
    My0=r.My0
if r.Mz0<0:
    Mz0= (r.Nz-r.Mz)/2
else:
    Mz0=r.Mz0

Yeeshift = False
fromlist = [r.l1,r.l2]    
A = interp_erosion(r.Mx,r.My,r.Mz,r.Nx,r.Ny,r.Nz,Mx0,My0,Mz0,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=Yeeshift,fromlist=fromlist)
Atop = None
if r.withtop>0:
    Atop = interp_erosion(r.Mx,r.My,r.withtop,r.Nx,r.Ny,r.Nz,Mx0,My0,Mz0+r.Mz,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=Yeeshift)
    
if r.epsfile == 'vac':
    dofo=np.zeros(r.Mxo*r.Myo).astype(np.float)
else:
    f = open(r.epsfile, 'r')
    dofo = np.loadtxt(f)

if r.Mxo == r.Mx and r.Myo == r.My:
    dofnew = dofo
else:
    xo, yo = np.mgrid[0:r.Mx:r.Mxo*1j, 0:r.My:r.Myo*1j]
    x, y = np.mgrid[0:r.Mx:r.Mx*1j, 0:r.My:r.My*1j]
    points = np.vstack([xo.flatten(),yo.flatten()]).transpose()
    dofnew = griddata(points,dofo, (x,y), method='nearest')
print "The size of the problem is",shape
# epsilon profile
epsBkg = np.ones(shape)*r.epsbkg
if r.Mzsub0<0:
    epsBkg[:,:,:A.Mz0] = r.epssub
else:
    epsBkg[:,:,r.Mzsub0:A.Mz0] = r.epssub

ep = [np.copy(epsBkg) for i in range(3)]
if r.with_wvg == 1:
    xadd = 1
    Awvg = interp_erosion(r.wvg_tx+2*xadd,r.wvg_ty,r.wvg_tz,r.Nx,r.Ny,r.Nz,r.wvg_x0-xadd,r.wvg_y0,r.wvg_z0,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=False,fromlist=fromlist)
    dofwvg = np.zeros([r.wvg_tx+2*xadd,r.wvg_ty])
    if xadd>0:
        dofwvg[xadd:-xadd,:]=1
    else:
        dofwvg[:,:]=1    
    Awvg.matAfromlist(dofwvg,ep,r.epsdiff)
    if r.withtop>0:
        Atopw = interp_erosion(r.wvg_tx+2*xadd,r.wvg_ty,r.withtop,r.Nx,r.Ny,r.Nz,r.wvg_x0-xadd,r.wvg_y0,r.wvg_z0+r.Mz,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=False)
        doftop = dofwvg
        if r.l2>0:
            Ero = Erosion(Mx=r.wvg_tx+2*xadd,My=r.wvg_ty,scale=r.scale)
            doftop = Ero.f_erosion(dofwvg)
        _ = Atopw.matA(doftop,ep,r.epssub-1)

if r.with_wvg2 == 1:
    xadd = 1
    Awvg = interp_erosion(r.wvg2_tx+2*xadd,r.wvg2_ty,r.wvg2_tz,r.Nx,r.Ny,r.Nz,r.wvg2_x0-xadd,r.wvg2_y0,r.wvg2_z0,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=False,fromlist=fromlist)
    dofwvg = np.zeros([r.wvg2_tx+2*xadd,r.wvg2_ty])
    if xadd>0:
        dofwvg[xadd:-xadd,:]=1
    else:
        dofwvg[:,:]=1    
    Awvg.matAfromlist(dofwvg,ep,r.epsdiff)
    if r.withtop>0:
        Atopw = interp_erosion(r.wvg2_tx+2*xadd,r.wvg2_ty,r.withtop,r.Nx,r.Ny,r.Nz,r.wvg2_x0-xadd,r.wvg2_y0,r.wvg2_z0+r.Mz,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=False)
        doftop = dofwvg
        if r.l2>0:
            Ero = Erosion(Mx=r.wvg2_tx+2*xadd,My=r.wvg2_ty,scale=r.scale)
            doftop = Ero.f_erosion(dofwvg)
        _ = Atopw.matA(doftop,ep,r.epssub-1)                    

A.matAfromlist(dofnew,ep,r.epsdiff)

if r.withtop>0:
    doftop = dofnew
    if r.l2>0:
        Ero = Erosion(Mx=r.Mx,My=r.My,scale=r.scale)
        doftop = Ero.f_erosion(dofnew)
    _ = Atop.matA(doftop,ep,r.epssub-1)

outpol = 0
if r.testYee>=0:
    outpol = r.testYee
    ep[r.testYee][:,:,1:] = 0.5*(ep[r.testYee][:,:,0:-1]+ep[r.testYee][:,:,1:])
    
f=h5py.File(r.name+'epsF.h5','w')
f.create_dataset('data',data=ep[outpol])
f.close()
