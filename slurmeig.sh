#!/bin/bash
#SBATCH --job-name=test
#SBATCH --output=test.txt
#SBATCH --gres=gpu:2
#SBATCH -N 1
#SBATCH --ntasks-per-node=2
#SBATCH --time=01:00:00
#SBATCH --mem=2500
#SBATCH --error=test.err
##SBATCH --mail-type=begin
##SBATCH --mail-type=end
#SBATCH --mail-user=weiliang@princeton.edu

module load anaconda/5.3.0 openmpi/gcc/2.1.0/64 cudatoolkit/9.0 intel/17.0/64/17.0.5.239

prop=eig.py
np=2
freq=1
Qabs=1e16
neig=2
Nmax=2
eigtol=1e-4
qrtol=1e-10

Nx=100
Ny=100
Nz=70
Mx=50
My=50
Mz=20
Mzslab=1

hx=0.02
hy=0.02
hz=0.02

Npmlx=10
Npmly=10
Npmlz=10

epsbkg=1
epssub=1
epsdiff=11
epstype='file'
epsfile='c.m'

Jamp=1.0
Jdir=2
cx=$(($Nx/2))
cy=$(($Ny/2))
cz=$(($Nz/2))
rx=5
ry=5
rz=5

# solver
maxit=50000
solverbase=1000
tol=1e-5
verbose=1
init=2
init_type='zero'

fpre='mode_Mx$Mx-My$My-Mz$Mz-f$freq-Q$Qabs-n$neig-N$Nmax-tol$eigtol.'
name=`hostname`
ID=$$
outputfile=$name-$fpre.$ID-out

eval "mpiexec -n $np python $prop -name $fpre -Job $Job -freq $freq -Qabs $Qabs -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mzslab $Mzslab -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg $epsbkg -epssub $epssub -epsdiff $epsdiff -epstype $epstype -epsfile $epsfile -Jamp $Jamp -Jdir $Jdir -cx $cx -cy $cy -cz $cz -rx $rx -ry $ry -rz $rz -maxit $maxit -tol $tol -verbose $verbose -solverbase $solverbase -init_type $init_type -Nmax $Nmax -neig $neig -eigtol $eigtol -qrtol $qrtol >$outputfile"
