import numpy as np

#freq = np.linspace(1.2,1.2,1)
lam = np.array([8., 9., 10., 11., 12., 13.]);
freq = 10./lam;
theta = np.linspace(-89,89,40)
phi = np.linspace(0,90,30)
freq,theta,phi = np.meshgrid(freq,theta,phi,indexing='ij')

fout = np.array([freq.flatten(),theta.flatten(),phi.flatten()])
fout = np.transpose(fout)
np.savetxt('freqfile_phimore_lam8to13.txt',fout)
