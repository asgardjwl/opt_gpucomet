#!/bin/bash
#SBATCH --job-name=0
#SBATCH --output=test.txt
#SBATCH --gres=gpu:2
#SBATCH -N 1
#SBATCH --ntasks-per-node=2
#SBATCH --time=24:00:00
#SBATCH --mem=20000
#SBATCH --error=test.err
#SBATCH --mail-type=begin
#SBATCH --mail-type=end
#SBATCH --mail-user=wljin@stanford.edu

module load anaconda/5.3.0 openmpi/cuda-9.0/gcc/3.0.0/64 cudatoolkit/9.0
#module load anaconda/5.3.0 openmpi/cuda-8.0/gcc/2.1.0/64 cudatoolkit/8.0

prop=angular.py
Job=1
np=2
readfile='freqfile_phimore_tmp2.txt'
shape='rec2'
lam0=10 # in unit of micron
s_amp=1
p_amp=0

hx=0.05
hy=$hx
hz=$hx

scale=1
Lx0=80 # 60-200
#Lx10=14.56 # 60-200
Lx10=0.5 # 60-200
Lgap0=20 
Lsub0=100
Lh0=40 #40-110
Lthick0=1 #0.2-1

fpre="lam8to13-Al-PDMS-phi-$shape-scale$scale-L$lam0-s$s_amp-p$p_amp-hx$hx-Lx$Lx0-Lx$Lx10-Lsub$Lsub0-Lh$Lh0-Lthick$Lthick0-Lgap$Lgap0-part3"

Lx=$(echo "$scale*$Lx0/$lam0"|bc -l)
Lx1=$(echo "$scale*$Lx10/$lam0"|bc -l)
Lgap=$(echo "$Lgap0/$lam0"|bc -l)
Lsub=$(echo "$Lsub0/$lam0+$Lgap"|bc -l)
Lh=$(echo "$scale*$Lh0/$lam0"|bc -l)
Lthick=$(echo "$Lthick0/$lam0"|bc -l)
Lsubvac=$Lgap
Ly=$(echo "2*$Lgap+$Lsub+$Lh"|bc)

cy=$(echo "($Ly-1)/$hy"|bc)
cyprobe=$(echo "1/$hy"|bc)

Nx=$(echo "$Lx/$hx"|bc)
Nx1=$(echo "$Lx1/$hx"|bc)
Ny=$(echo "$Ly/$hy"|bc)
Nsub=$(echo "$Lsub/$hx"|bc)
Nsubvac=$(echo "$Lsubvac/$hx"|bc)
Nh=$(echo "$Lh/$hy"|bc)
Nthick=$(echo "$Lthick/$hy"|bc)
Nz=1

Npmlx=0
Npmly=10
Npmlz=0

# solver
maxit=60000
tol=1e-3
verbose=1
init=2
init_type='rand'
solverbase=10000

name=`hostname`
ID=$$
outputfile=$name-$fpre.$ID-out

eval "mpiexec -n $np python $prop -Nx $Nx -Ny $Ny -Nz $Nz \
-hx $hx -hy $hy -hz $hz -shape $shape \
-Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz \
-cy $cy -cyprobe $cyprobe -Job $Job \
-maxit $maxit -tol $tol -verbose $verbose -solverbase $solverbase \
-init $init -init_type $init_type \
-s_amp $s_amp -p_amp $p_amp \
-readfile $readfile -lam0 $lam0 \
-Nsub $Nsub -Nsubvac $Nsubvac -Nh $Nh -Nthick $Nthick -Nx1 $Nx1 \
>$outputfile"
