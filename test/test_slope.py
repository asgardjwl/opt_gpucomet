import numpy as np
from scipy import ndimage
import h5py

class interp:
    def __init__(self,Mx,My,Mz,Mzslab,Nx,Ny,Nz,Mx0,My0,Mz0,rep=1,angle=0):
        '''
        angle = 0: verticle; >0: narrowing up
        '''
        self.Mx=Mx
        self.My=My
        self.Mz=Mz
        self.Mzslab=Mzslab
        self.Mx0=Mx0
        self.My0=My0
        self.Mz0=Mz0
        self.Nx=Nx
        self.Ny=Ny
        self.Nz=Nz
        self.rep = rep
        self.slope = np.tan(angle*np.pi/180.)
        
    def matA(self,dof,ep,epsdiff):
        if self.Mzslab is 1:
            if int(round(self.slope*self.Mz/self.rep))<1:
                for i in range(self.Mz0,self.Mz0+self.Mz):
                    for kx in range(self.rep):
                        for ky in range(self.rep):
                            ep[kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,i] += dof*epsdiff

            else:
                dofi = np.round(dof)
                narrowp = 0
                for i in range(self.Mz0+self.Mz-1,self.Mz0-1,-1):
                    narrow = int(round(self.slope*(self.Mz0+self.Mz-1-i)/self.rep))
                    if narrow>narrowp:
                        dofi = ndimage.binary_dilation(dofi).astype(dofi.dtype)
                    for kx in range(self.rep):
                        for ky in range(self.rep):
                            ep[kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,i] += dofi*epsdiff
                    narrowp = narrow
        else:
            for kx in range(self.rep):
                for ky in range(self.rep):
                    for kz in range(self.rep):            
                        ep[kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,kz+self.Mz0:self.Mz0+self.Mz:self.rep] += dof*epsdiff
                        
    def matAT(self,dof,e,factor=1.):
        if self.Mzslab is 1:
            for i in range(self.Mz0,self.Mz0+self.Mz):
                narrow = int(round(self.slope*(i-self.Mz0)/self.rep))
                
                for j in range(3):
                    for kx in range(self.rep):
                        for ky in range(self.rep):
                            dof += factor*e[j][kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,i]
        else:
            for j in range(3):
                for kx in range(self.rep):
                    for ky in range(self.rep):
                        for kz in range(self.rep):                
                            dof += factor*e[j][kx+self.Mx0:self.Mx0+self.Mx:self.rep,ky+self.My0:self.My0+self.My:self.rep,kz+self.Mz0:self.Mz0+self.Mz:self.rep]
                            

Mx = 150
My = 200
Mz = 16
Mzslab = 1
Nx = 250
Ny = 300
Nz = 36
Mx0 = 50
My0 = 50
Mz0 = 10
rep = 1

shape = (Nx,Ny,Nz)
ep = np.ones(shape)

A = interp(Mx,My,Mz,Mzslab,Nx,Ny,Nz,Mx0,My0,Mz0,rep=rep,angle=15)

f = open('epsb150R4al2_b.m', 'r')
dof = np.loadtxt(f).reshape(Mx/rep,My/rep)
A.matA(dof,ep,10.)

f=h5py.File('epsF.h5','w')
f.create_dataset('data',data=ep)
f.close()
