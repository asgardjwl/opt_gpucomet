import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi
#from pycuda import driver

from lib.tool import conditioners
from lib.tool import write_results
from lib.tool import PML
from lib.tool import interp
from lib.operators import rho_step
from lib.operators import alpha_step
from lib.bicg import bicg
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# solvers
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-6)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
# temporal
parser.add_argument('-freq', action="store", type=float, default=1.0)
parser.add_argument('-Qabs', action="store", type=float, default=1e16)
# geometry
parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mx', action="store", type=int, default=25)
parser.add_argument('-My', action="store", type=int, default=25)
parser.add_argument('-Mz', action="store", type=int, default=20)
parser.add_argument('-Mzslab', action="store", type=int, default=1)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epsdiff', action="store", type=float, default=11.0)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='vac')

# current source
parser.add_argument('-Jamp', action="store", type=float, default=1.0)
parser.add_argument('-Jdir', action="store", type=int, default=2)
parser.add_argument('-cx', action="store", type=int, default=37)
parser.add_argument('-cy', action="store", type=int, default=37)
parser.add_argument('-cz', action="store", type=int, default=35)

r=parser.parse_args(sys.argv[1:]);
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.maxit=r.maxit
verbose.tol=r.tol
# ----------assembling parameters------------#
if r.Qabs>1e10:
    r.Qabs=float('inf')
    
omega = dtype(r.freq*2*pi) - 1j*dtype(r.freq*2*pi/2/r.Qabs)
shape = (r.Nx,r.Ny,r.Nz)
pmlx=PML(r.Npmlx,omega,r.Nx,r.hx)
pmly=PML(r.Npmly,omega,r.Ny,r.hy)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz)

pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]
# pml
if comm.rank == 0:
    print "The size of the problem is",shape
    
    mu=dtype([np.ones(shape),np.ones(shape),np.ones(shape)]);#mu
    # current source
    j=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)]);
    j[r.Jdir][r.cx,r.cy,r.cz]=r.Jamp/r.hx/r.hy #electric current

    # epsilon profile
    ndof=r.Mx*r.My
    if r.epstype is 'one':
        dof=np.ones([r.Mx,r.My]).astype(np.float)
    elif r.epstype is 'vac':
        dof=np.zeros([r.Mx,r.My]).astype(np.float)
    elif r.epstype is 'rand':
        dof=np.random.randn(r.Mx,r.My).astype(np.float)
    elif r.epstype is 'file':
        f = open(r.epsfile, 'r')
        dof = np.loadtxt(f).reshape(r.Mx,r.My)

    epsBkg = np.ones(shape)*r.epsbkg
    Mx0= (r.Nx-r.Mx)/2
    My0= (r.Ny-r.My)/2
    Mz0= (r.Nz-r.Mz)/2
    A = interp(r.Mx,r.My,r.Mz,r.Mzslab,r.Nx,r.Ny,r.Nz,Mx0,My0,Mz0)

    ep = np.copy(epsBkg)
    t1 = time.time()
    A.matA(dof,ep,r.epsdiff)
    t2 = time.time()
    print "time for interp=",t2-t1

    # isotropic
    e=dtype([ep,ep,ep])

    # initial solution
    #x=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    x=dtype([np.random.randn(*shape),np.random.randn(*shape),np.random.randn(*shape)])
    b=[np.copy(z) for z in j[:]] #b=-i omega j
    for k in range(3):
        mu[k] = mu[k]**-1
        e[k] = omega**2 * e[k]
        b[k] = -1j * omega * b[k]
        
else:
    e=[None] * 3
    mu=[None] * 3
    j=[None] * 3
    x=[None] * 3
    b=[None] * 3
        
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)
if comm.rank==0:
    pre_cond, post_cond = conditioners(pml_p,pml_d, dtype)
    b=pre_cond(b) #preconditioner
else:
    pre_cond = None
    post_cond = None
    
# GPU vectors
bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in b]
xg = [Grid(dtype(f), x_overlap=1) for f in x]
ops = {'zeros': lambda: [Grid(dtype, x_overlap=1) for k in range(3)], \
            'rho_step': rho_step(dtype), \
            'alpha_step': alpha_step(shape,pml_p,pml_d,e,mu,dtype)}
#-------solve-----------#
t1 = time.time()
# xg, err, success = bicg(bg, x=xg, \
#                         max_iters=verbose.maxit, \
#                         err_thresh=verbose.tol, \
#                         **ops)
# e=[E.get() for E in xg]

from test import solve, test
from lib.tool import memory_report
import gc

memory_report()
for i in range(500):
    solve(b,x,verbose.maxit,verbose.tol,ops)
    gc.collect()
    memory_report(str(i)+'-')
