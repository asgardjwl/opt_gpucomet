import numpy as np
from mpi4py.MPI import COMM_WORLD as comm

import sys
sys.path.append('..')
from lib.tool import memory_report
from lib.tool import PML
from lib.operators import alpha_step
from lib.tool import PML
from lib.gce.space import initialize_space
from lib.gce.grid import Grid

dtype=np.complex128
shape=(100,100,100)

initialize_space(shape)

pmlx=PML(10,1,100,0.01)
pmly=PML(10,1,100,0.01)
pmlz=PML(10,1,100,0.01)

pml_p=[pmlx.sp,pmly.sp,pmlz.sp]
pml_d=[pmlx.sd,pmly.sd,pmlz.sd]

if comm.rank is 0:
    mu=dtype([np.ones(shape),np.ones(shape),np.ones(shape)]);#mu
    ep=dtype([np.ones(shape),np.ones(shape),np.ones(shape)]);#ep
else:
    mu=[None] * 3
    ep=[None] * 3

f_alpha=alpha_step(shape,pml_p,pml_d,ep,mu,dtype)

x=dtype([np.random.randn(*shape),np.random.randn(*shape),np.random.randn(*shape)])
v = [Grid(dtype(f), x_overlap=1) for f in x]
p = [Grid(dtype(f), x_overlap=1) for f in x]
bg = [Grid(dtype(np.copy(f)), x_overlap=1) for f in x]
xg = [Grid(dtype(f), x_overlap=1) for f in x]

for i in range(50000):
    alpha=f_alpha(0.0001, 1, p, bg, v)
    memory_report(str(i)+'-'+str(comm.rank)+'-')

