#!/bin/bash
#SBATCH --job-name=test
#SBATCH --output=test.txt
#SBATCH --gres=gpu:2
#SBATCH -N 1
#SBATCH --ntasks-per-node=2
#SBATCH --time=00:01:00
#SBATCH --mem-per-cpu=400
#SBATCH --error=test.err
##SBATCH --mail-type=begin
##SBATCH --mail-type=end
#SBATCH --mail-user=weiliang@princeton.edu

module load anaconda/5.3.0 openmpi/gcc/2.1.0/64 cudatoolkit/9.0 intel/17.0/64/17.0.5.239

prop=test_alpha.py
np=2

mpiexec -n $np python $prop
