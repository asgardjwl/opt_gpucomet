import h5py
import numpy as np
from mpi4py.MPI import COMM_WORLD as comm
import time, sys, argparse
from math import pi

from lib.tool import write_results, PML
from lib.gce.grid import Grid
from lib.gce import verbose
from lib.gce.space import initialize_space
from lib.solver import Solver
from lib.operators import M_step
from lib.eigmode import arnoldi_eig, krylov_vec
from lib.erosion import interp_erosion

dtype=np.complex128
#------------- passing argument---------------#
parser = argparse.ArgumentParser()
# job
# 0 for krylov vectors only
parser.add_argument('-Job', action="store", type=int, default=1)
parser.add_argument('-rep', action="store", type=int, default=1)
# solvers
parser.add_argument('-name', action="store", type=str, default='test')
parser.add_argument('-maxit', action="store", type=int, default=100000)
parser.add_argument('-tol', action="store", type=float, default=1e-6)
parser.add_argument('-verbose', action="store", type=int, default=1)
parser.add_argument('-solverbase', action="store", type=int, default=100)
parser.add_argument('-init_type', action="store", type=str, default='zero')
# eig
parser.add_argument('-neig', action="store", type=int, default=1)
parser.add_argument('-Nmax', action="store", type=int, default=10)
parser.add_argument('-eigtol', action="store", type=float, default=1e-3)
parser.add_argument('-qrtol', action="store", type=float, default=1e-10)
# temporal
parser.add_argument('-freq', action="store", type=float, default=1.0)
parser.add_argument('-Qabs', action="store", type=float, default=200)
# geometry
parser.add_argument('-Nx', action="store", type=int, default=75)
parser.add_argument('-Ny', action="store", type=int, default=75)
parser.add_argument('-Nz', action="store", type=int, default=70)

parser.add_argument('-Mx', action="store", type=int, default=25)
parser.add_argument('-My', action="store", type=int, default=25)
parser.add_argument('-Mz', action="store", type=int, default=20)
parser.add_argument('-Mzsub0', action="store", type=int, default=-1)
parser.add_argument('-Mx0', action="store", type=int, default=-1)
parser.add_argument('-My0', action="store", type=int, default=-1)
parser.add_argument('-Mz0', action="store", type=int, default=-1)

parser.add_argument('-Npmlx', action="store", type=int, default=10)
parser.add_argument('-Npmly', action="store", type=int, default=10)
parser.add_argument('-Npmlz', action="store", type=int, default=10)

parser.add_argument('-pml_m', action="store", type=float, default=4.0)
parser.add_argument('-pml_R', action="store", type=float, default=16.0)
parser.add_argument('-pml_pd', action="store", type=int, default=1)

parser.add_argument('-hx', action="store", type=float, default=0.02)
parser.add_argument('-hy', action="store", type=float, default=0.02)
parser.add_argument('-hz', action="store", type=float, default=0.02)

# erosion
parser.add_argument('-angle', action="store", type=float, default=0.0)
parser.add_argument('-wvgangle', action="store", type=float, default=-1)
parser.add_argument('-scale', action="store", type=float, default=100.0)
parser.add_argument('-erobproj', action="store", type=float, default=0.0)
parser.add_argument('-mode', action="store", type=str, default='dilation')
parser.add_argument('-Yeeshift', action="store", type=int, default=1)
parser.add_argument('-l1', action="store", type=int, default=0)
parser.add_argument('-l2', action="store", type=int, default=0)
# dielectric
parser.add_argument('-epsbkg', action="store", type=float, default=1.0)
parser.add_argument('-epssub', action="store", type=float, default=1.0)
parser.add_argument('-epssuby', action="store", type=float, default=-1.0)
parser.add_argument('-epssubz', action="store", type=float, default=-1.0)
parser.add_argument('-epsdiff', action="store", type=float, default=11.0)
parser.add_argument('-epsdiffy', action="store", type=float, default=-1)
parser.add_argument('-epsdiffz', action="store", type=float, default=-1)
parser.add_argument('-epsfile', action="store", type=str, default='init.txt')
parser.add_argument('-epstype', action="store", type=str, default='vac')
# current source
parser.add_argument('-Jamp', action="store", type=float, default=1.0)
parser.add_argument('-Jdir', action="store", type=int, default=2)
parser.add_argument('-cx', action="store", type=int, default=37)
parser.add_argument('-cy', action="store", type=int, default=37)
parser.add_argument('-cz', action="store", type=int, default=35)
parser.add_argument('-rx', action="store", type=int, default=2)
parser.add_argument('-ry', action="store", type=int, default=2)
parser.add_argument('-rz', action="store", type=int, default=2)

# wvg
parser.add_argument('-with_wvg', action="store", type=int, default=0)
parser.add_argument('-wvg_x0', action="store", type=int, default=37)
parser.add_argument('-wvg_y0', action="store", type=int, default=37)
parser.add_argument('-wvg_z0', action="store", type=int, default=35)
parser.add_argument('-wvg_tx', action="store", type=int, default=1)
parser.add_argument('-wvg_ty', action="store", type=int, default=1)
parser.add_argument('-wvg_tz', action="store", type=int, default=1)
parser.add_argument('-with_wvg2', action="store", type=int, default=0)
parser.add_argument('-wvg2_x0', action="store", type=int, default=37)
parser.add_argument('-wvg2_y0', action="store", type=int, default=37)
parser.add_argument('-wvg2_z0', action="store", type=int, default=35)
parser.add_argument('-wvg2_tx', action="store", type=int, default=1)
parser.add_argument('-wvg2_ty', action="store", type=int, default=1)
parser.add_argument('-wvg2_tz', action="store", type=int, default=1)

r, unknown = parser.parse_known_args(sys.argv[1:])
if comm.rank==0:
    for arg in vars(r):
        print arg," is ",getattr(r,arg)
        
verbose.v=r.verbose
verbose.solverbase=r.solverbase
verbose.filename=r.name
verbose.init_type=r.init_type
# ----------assembling parameters------------#
hxyz=r.hx*r.hy*r.hz
if r.Qabs>1e10:
    r.Qabs=float('inf')
    
omega = dtype(r.freq*2*pi) - 1j*dtype(r.freq*2*pi/2/r.Qabs)
shape = (r.Nx,r.Ny,r.Nz)
# pml
pmlx=PML(r.Npmlx,omega,r.Nx,r.hx,r.pml_m,r.pml_R)
pmly=PML(r.Npmly,omega,r.Ny,r.hy,r.pml_m,r.pml_R)
pmlz=PML(r.Npmlz,omega,r.Nz,r.hz,r.pml_m,r.pml_R)

pml_p=[pmlx.sp*r.hx,pmly.sp*r.hy,pmlz.sp*r.hz]
pml_d=[pmlx.sd*r.hx,pmly.sd*r.hy,pmlz.sd*r.hz]

#interp matrix
ndof=(r.Mx/r.rep)*(r.My/r.rep)
if r.Mx0<0:
    Mx0= (r.Nx-r.Mx)/2
else:
    Mx0=r.Mx0
if r.My0<0:
    My0= (r.Ny-r.My)/2
else:
    My0=r.My0
if r.Mz0<0:
    Mz0= (r.Nz-r.Mz)/2
else:
    Mz0=r.Mz0

if r.Yeeshift == 0:
    Yeeshift = False
else:
    Yeeshift = True

fromlist = [r.l1,r.l2]
if r.mode == 'dilation':
    r.scale = -r.scale
    
A = interp_erosion(r.Mx,r.My,r.Mz,r.Nx,r.Ny,r.Nz,Mx0,My0,Mz0,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=Yeeshift,mode=r.mode,fromlist=fromlist)

Aout = interp_erosion(r.Mx,r.My,r.Mz,r.Nx,r.Ny,r.Nz,Mx0,My0,Mz0,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=False,mode=r.mode,fromlist=fromlist)

if r.epstype == 'one':
    dof=np.ones([r.Mx/r.rep,r.My/r.rep]).astype(np.float)
elif r.epstype == 'rand':
    dof=np.random.rand(r.Mx/r.rep,r.My/r.rep).astype(np.float)
elif r.epstype == 'file':
    f = open(r.epsfile, 'r')
    dof = np.loadtxt(f).reshape(r.Mx/r.rep,r.My/r.rep)
elif r.epstype == 'vac':
    dof=np.zeros([r.Mx/r.rep,r.My/r.rep]).astype(np.float)

if r.epsdiffy <0 and r.epsdiffz <0:
    epsdiff = r.epsdiff
else:
    epsdiff = [r.epsdiff,r.epsdiffy,r.epsdiffz]

if r.epssuby <0 and r.epssubz <0:
    epssub = [r.epssub,r.epssub,r.epssub]
else:
    epssub = [r.epssub,r.epssuby,r.epssubz]
    
j=[None] * 3
x=[None] * 3
b=[None] * 3
ep=[None] * 3
epout=[None] * 3
if comm.rank == 0:
    print "The size of the problem is",shape
    # current source
    j=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)]);
    if r.rz>0:
        j[r.Jdir][r.cx-r.rx:r.cx+r.rx,r.cy-r.ry:r.cy+r.ry,r.cz-r.rz:r.cz+r.rz]=np.random.rand(2*r.rx,2*r.ry,2*r.rz)
    else:
        j[r.Jdir][r.cx-r.rx:r.cx+r.rx,r.cy-r.ry:r.cy+r.ry,r.cz]=np.random.rand(2*r.rx,2*r.ry)

    # epsilon profile
    epsBkg = [np.ones(shape)*r.epsbkg for i in range(3)]
    for i in range(3):
        if r.Mzsub0<0:
            epsBkg[i][:,:,:A.Mz0] = epssub[i]
        else:
            epsBkg[i][:,:,r.Mzsub0:A.Mz0] = epssub[i]

    ep = [np.copy(f) for f in epsBkg]

    if r.with_wvg == 1:
        if r.wvgangle<0:
            xadd = int(round(A.slope*r.wvg_tz))
        else:
            slope = np.tan(r.wvgangle*np.pi/180.)
            xadd = int(round(slope*r.wvg_tz))
        if r.l2>0:
            xadd = 1
        Awvg = interp_erosion(r.wvg_tx+2*xadd,r.wvg_ty,r.wvg_tz,r.Nx,r.Ny,r.Nz,r.wvg_x0-xadd,r.wvg_y0,r.wvg_z0,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=False,mode=r.mode,fromlist=fromlist)
        dofwvg = np.zeros([r.wvg_tx+2*xadd,r.wvg_ty])
        if xadd>0:
            dofwvg[xadd:-xadd,:]=1
        else:
            dofwvg[:,:]=1
            
        if r.l2>0:
            Awvg.matAfromlist(dofwvg,ep,epsdiff)
        else:
            _ = Awvg.matA(dofwvg,ep,epsdiff)

    if r.with_wvg2 == 1:
        if r.wvgangle<0:
            xadd = int(round(A.slope*r.wvg2_tz))
        else:
            slope = np.tan(r.wvgangle*np.pi/180.)
            xadd = int(round(slope*r.wvg2_tz))
        if r.l2>0:
            xadd = 1
        Awvg = interp_erosion(r.wvg2_tx+2*xadd,r.wvg2_ty,r.wvg2_tz,r.Nx,r.Ny,r.Nz,r.wvg2_x0-xadd,r.wvg2_y0,r.wvg2_z0,angle=r.angle,scale=r.scale,bproj=r.erobproj,Yee=False,mode=r.mode,fromlist=fromlist)
        dofwvg = np.zeros([r.wvg2_tx+2*xadd,r.wvg2_ty])
        if xadd>0:
            dofwvg[xadd:-xadd,:]=1
        else:
            dofwvg[:,:]=1
            
        if r.l2>0:
            Awvg.matAfromlist(dofwvg,ep,epsdiff)
        else:
            _ = Awvg.matA(dofwvg,ep,epsdiff)
            
    epout = [np.copy(f) for f in ep]
    if r.l2>0:
        A.matAfromlist(dof,ep,epsdiff)
        Aout.matAfromlist(dof,epout,epsdiff)
    else:
        _ = A.matA(dof,ep,epsdiff)
        _ = Aout.matA(dof,epout,epsdiff)

    if r.epssuby <0 and r.epssubz <0:
        f=h5py.File(r.name+'epsF.h5','w')
        f.create_dataset('data',data=epout[0])
        f.close()
    else:
        f=h5py.File(r.name+'epsFx.h5','w')
        f.create_dataset('data',data=epout[0])
        f.close()

        f=h5py.File(r.name+'epsFy.h5','w')
        f.create_dataset('data',data=epout[1])
        f.close()

        f=h5py.File(r.name+'epsFz.h5','w')
        f.create_dataset('data',data=epout[2])
        f.close()        

    # initial solution
    x=dtype([np.zeros(shape),np.zeros(shape),np.zeros(shape)])
    b=[-1j*omega*np.copy(z) for z in j] #b=-i omega j
    
#-------opertors----------#
#initiliaze GPU
initialize_space(shape)
if r.pml_pd == 1:
    solver = Solver(shape, r.tol, r.maxit, pml_p, pml_d, omega)
else:
    solver = Solver(shape, r.tol, r.maxit, pml_d, pml_d, omega)
    
solver.update_ep(ep)

epw2 = [None]*3
muinv=[None]*3
if comm.rank==0:
    muinv = [np.ones(shape),np.ones(shape),np.ones(shape)]
    epw2 = [omega**2 * f for f in ep]
M = M_step(shape,pml_p,pml_d,epw2,muinv,dtype)

# eigensolver
if r.Job == 1:
    eigfreq, Qn = arnoldi_eig(ep, b, solver, M, r.neig, r.Nmax, r.eigtol, r.qrtol, r.init_type)
    for i in range(r.neig):
        if comm.rank == 0:
            write_results(r.name+'x'+str(i)+'.h5',Qn[i])
elif r.Job == 0:
    krylov_vec(ep, b, solver, r.Nmax, r.init_type)
