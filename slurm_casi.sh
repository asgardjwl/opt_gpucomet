#!/bin/bash
#SBATCH --job-name=test
#SBATCH --output=chi10.txt
#SBATCH --gres=gpu:1
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=02:00:00
#SBATCH --mem=20000
#SBATCH --error=test.err
##SBATCH --mail-type=begin
##SBATCH --mail-type=end
#SBATCH --mail-user=weiliang@princeton.edu

module load anaconda/5.3.0 openmpi/gcc/2.1.0/64 cudatoolkit/8.0
#module load anaconda/5.3.0 openmpi/cuda-8.0/gcc/2.1.0/64 cudatoolkit/9.0

prop=fdfd_casimir.py
np=1

hx=0.025
hy=$hx
hz=$hx

Lx=3
Ly=3
Lz=3
d=1

Nx=$(echo "$Lx/$hx" | bc);
Ny=$(echo "$Ly/$hy" | bc);
Nz=$(echo "($Lz+$d)/$hz" | bc);

Mx=$Nx
My=$Ny
Mz=$(echo "$Lz/$hz/2" | bc);

Mx0=0
My0=0
Mz0=0
Mzslab=1

Npmlx=10
Npmly=10
Npmlz=10

epsbkg=1
epsdiff=10
epstype='one'
epsfile='c.txt'

Jamp=1.0
Jdir=2
cx=$(($Nx/2))
cy=$(($Ny/2))
cz=$(echo "$Mz+$d/$hz" | bc);

# solver
maxit=100000
tol=1e-6
verbose=1
init=2
init_type='rand'
solverbase=5000

mpiexec -n $np python $prop -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mx0 $Mx0 -My0 $My0 -Mz0 $Mz0 -Mzslab $Mzslab -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg $epsbkg -epsdiff $epsdiff -epstype $epstype -epsfile $epsfile -Jamp $Jamp -Jdir $Jdir -cx $cx -cy $cy -cz $cz -maxit $maxit -tol $tol -verbose $verbose -solverbase $solverbase -init $init -init_type $init_type
