#!/bin/bash
#SBATCH --job-name=test
#SBATCH --output=test.txt
#SBATCH -p gpu-shared
#SBATCH --gres=gpu:1
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=00:30:00
#SBATCH --mem=24000
#SBATCH --error=test.err

export PATH="/home/asgard/miniconda2/bin:$PATH"
module load cuda openmpi_ib

prop=fdfd.py
np=1
Job=0
freq=1
Qabs=1e16
xsi=0

kx=0.0
ky=0.0
kz=0.0
PECx=1
PECy=0
PECz=0

Nx=50
Ny=50
Nz=100
Mx=10
My=10
Mz=50
Mzslab=1

hx=0.02
hy=0.02
hz=0.02

Npmlx=10
Npmly=10
Npmlz=10

epsbkg=1
epsdiff=6
epstype='vac'
epsfile='c.txt'

Jamp=1.0
Jdir=2
cx=$(($Nx/2))
cy=$(($Ny/2))
cz=$(($Nz/2))

# solver
maxit=10000
tol=1e-5
verbose=1
init=2
init_type='rand'

# nlopt
maxeval=5

outputbase=2
solverbase=1000

mpiexec -n $np python $prop -Job $Job -freq $freq -Qabs $Qabs -Nx $Nx -Ny $Ny -Nz $Nz -Mx $Mx -My $My -Mz $Mz -Mzslab $Mzslab -hx $hx -hy $hy -hz $hz -Npmlx $Npmlx -Npmly $Npmly -Npmlz $Npmlz -epsbkg $epsbkg -epsdiff $epsdiff -epstype $epstype -epsfile $epsfile -Jamp $Jamp -Jdir $Jdir -cx $cx -cy $cy -cz $cz -maxit $maxit -tol $tol -verbose $verbose -outputbase $outputbase -solverbase $solverbase -maxeval $maxeval -init $init -init_type $init_type -kx $kx -ky $ky -kz $kz -PECx $PECx -PECy $PECy -PECz $PECz -xsi $xsi
